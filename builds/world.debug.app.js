(function() {
  var App, Atomic, Commons, World, guid,
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; };

  World = {};

  World.Event = (function() {
    var inform, listen, _listeners;
    _listeners = {};
    listen = function(eventName, callback) {
      _listeners[eventName] = _listeners[eventName] || [];
      return _listeners[eventName].push(callback);
    };
    inform = function(eventName, data) {
      var listener, _i, _len, _ref, _results;
      if (!_listeners[eventName]) {
        return false;
      }
      _ref = _listeners[eventName];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        listener = _ref[_i];
        _results.push(listener.call(listener, data));
      }
      return _results;
    };
    return {
      listen: listen,
      inform: inform,
      listeners: function() {
        return _listeners;
      }
    };
  })();

  World.Item = (function() {

    /**
     * [constructor description]
     * @param  {[type]} @attributes={}
     * @param  {[type]} @children=[]
     * @return {[type]}
     */
    function Item(attributes, children) {
      var child, _i, _len, _ref;
      this.attributes = attributes != null ? attributes : {};
      this.children = children != null ? children : [];
      this.uid = guid();
      this.namespace = this.constructor.name;
      this.listeners = {};
      _ref = this.children;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        child.parent = this;
      }
    }


    /**
     * [appendChild description]
     * @param  {[type]} children
     * @return {[type]}
     */

    Item.prototype.appendChild = function(instance) {
      instance.parent = this;
      return this.children.push(instance);
    };


    /**
     * [bubble description]
     * @param  {[type]} event
     * @param  {[type]} data
     * @return {[type]}
     */

    Item.prototype.bubble = function(event_name, data, emmiter) {
      var result;
      if (data == null) {
        data = {};
      }
      if (emmiter == null) {
        emmiter = this;
      }
      if (this.parent) {
        result = this.parent.trigger(event_name, data, emmiter);
        if (!(this.preventBubbling === true || result === false)) {
          return this.parent.bubble(event_name, data, emmiter);
        }
      }
    };


    /**
     * [tunnel description]
     * @param  {[type]} eventName
     * @param  {[type]} data
     * @return {[type]}
     */

    Item.prototype.tunnel = function(event_name, data, emmiter) {
      var child, result, _i, _len, _ref, _results;
      if (data == null) {
        data = {};
      }
      if (emmiter == null) {
        emmiter = this;
      }
      _ref = this.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        result = child.trigger(event_name, data);
        if (!(child.preventTunneling === true || result === false)) {
          _results.push(child.tunnel(event_name, data, emmiter));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };


    /**
     * [on description]
     * @param  {[type]}   eventName
     * @param  {Function} callback
     * @return {[type]}
     */

    Item.prototype.on = function(eventName, callback) {
      this.listeners[eventName] = this.listeners[eventName] || [];
      this.listeners[eventName].push(callback);
      return this;
    };


    /**
     * [off description]
     * @param  {[type]}   eventName [description]
     * @param  {Function} callback  [description]
     * @return {[type]}             [description]
     */

    Item.prototype.off = function(eventName, callback) {
      var index;
      if (!this.listeners[eventName]) {
        return false;
      }
      index = this.listeners[eventName].indexOf(callback);
      if (index > -1) {
        this.listeners[eventName].splice(index, 1);
      }
      return this;
    };


    /**
     * [trigger description]
     * @param  {[type]} eventName [description]
     * @param  {[type]} data      [description]
     * @return {[type]}           [description]
     */

    Item.prototype.trigger = function(eventName, data, emmiter) {
      var callback, _i, _len, _ref;
      if (this.listeners[eventName]) {
        _ref = this.listeners[eventName];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          callback = _ref[_i];
          callback.call(this, data, emmiter);
        }
      }
      return this;
    };

    return Item;

  })();

  World.god = (function() {
    var WorldItem, create, items;
    items = {};
    WorldItem = (function() {

      /**
       * [constructor description]
       * @param  {[type]} @attributes={}
       * @param  {[type]} @children=[]
       * @return {[type]}
       */
      function WorldItem(attributes, children) {
        var child, _i, _len, _ref;
        this.attributes = attributes != null ? attributes : {};
        this.children = children != null ? children : [];
        this.uid = guid();
        this.namespace = this.constructor.name;
        this.listeners = {};
        _ref = this.children;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.parent = this;
        }
      }


      /**
       * [appendChild description]
       * @param  {[type]} children
       * @return {[type]}
       */

      WorldItem.prototype.appendChild = function(instance) {
        instance.parent = this;
        return this.children.push(instance);
      };


      /**
       * [bubble description]
       * @param  {[type]} event
       * @param  {[type]} data
       * @return {[type]}
       */

      WorldItem.prototype.bubble = function(event_name, data, emmiter) {
        var result;
        if (data == null) {
          data = {};
        }
        if (emmiter == null) {
          emmiter = this;
        }
        if (this.parent) {
          result = this.parent.trigger(event_name, data, emmiter);
          if (!(this.preventBubbling === true || result === false)) {
            return this.parent.bubble(event_name, data, emmiter);
          }
        }
      };


      /**
       * [tunnel description]
       * @param  {[type]} eventName
       * @param  {[type]} data
       * @return {[type]}
       */

      WorldItem.prototype.tunnel = function(event_name, data, emmiter) {
        var child, result, _i, _len, _ref, _results;
        if (data == null) {
          data = {};
        }
        if (emmiter == null) {
          emmiter = this;
        }
        _ref = this.children;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          result = child.trigger(event_name, data);
          if (!(child.preventTunneling === true || result === false)) {
            _results.push(child.tunnel(event_name, data, emmiter));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };


      /**
       * [listen description]
       * @param  {[type]}   eventName
       * @param  {Function} callback
       * @return {[type]}
       */

      WorldItem.prototype.listen = function(eventName, callback) {
        this.listeners[eventName] = this.listeners[eventName] || [];
        return this.listeners[eventName].push(callback);
      };


      /**
       * [unlisten description]
       * @param  {[type]}   eventName [description]
       * @param  {Function} callback  [description]
       * @return {[type]}             [description]
       */

      WorldItem.prototype.unlisten = function(eventName, callback) {
        var index;
        if (!this.listeners[eventName]) {
          return false;
        }
        index = this.listeners[eventName].indexOf(callback);
        if (index > -1) {
          return this.listeners[eventName].splice(index, 1);
        }
      };


      /**
       * [trigger description]
       * @param  {[type]} eventName [description]
       * @param  {[type]} data      [description]
       * @return {[type]}           [description]
       */

      WorldItem.prototype.trigger = function(eventName, data, emmiter) {
        var callback, _i, _len, _ref, _results;
        if (this.listeners[eventName]) {
          _ref = this.listeners[eventName];
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            callback = _ref[_i];
            _results.push(callback.call(callback, data, emmiter));
          }
          return _results;
        }
      };

      return WorldItem;

    })();
    create = function(attributes, children) {
      var item;
      item = new WorldItem(attributes, children);
      items[item.uid] = item;
      return item;
    };
    return {
      create: create
    };
  })();

  guid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r, v;
      r = Math.random() * 16 | 0;
      v = c === 'x' ? r : r & 3 | 8;
      return v.toString(16);
    }).toUpperCase();
  };

  Atomic = {
    DOM: $ || $$
  };

  Atomic.Class = (function(_super) {
    __extends(Class, _super);

    function Class(attributes, children, options) {
      this.attributes = attributes != null ? attributes : {};
      this.children = children != null ? children : [];
      this.options = options != null ? options : {};
      Class.__super__.constructor.apply(this, arguments);
      this.attributes.id = this.attributes.id || this.uid;
      if (this.constructor.template) {
        this.__createElementNode();
      }
    }


    /**
     * [render description]
     * @param  {[type]} container
     * @return {[type]}
     */

    Class.prototype.render = function(container) {
      var child, default_children_container, _i, _len, _ref, _results;
      this.container = container != null ? container : this.parent.el;
      if (!this.el) {
        this.__createElementNode();
      }
      this.container.append(this.el);
      if (this.children.length > 0) {
        default_children_container = this.__getDefaultContainer();
        _ref = this.children;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          if (child.attributes.container) {
            _results.push(child.render(this.__getChildContainer__(child.attributes.container)));
          } else {
            _results.push(child.render(default_children_container));
          }
        }
        return _results;
      }
    };

    Class.prototype.__createElementNode = function() {
      var callback, el, evt, parts, _ref, _results;
      this.el = Atomic.DOM(templayed(this.constructor.template)(this.attributes));
      if (this.events && this.el) {
        _ref = this.events;
        _results = [];
        for (evt in _ref) {
          callback = _ref[evt];
          parts = evt.split(" ");
          el = parts.length === 1 ? this.el : this.el.find(parts.slice(1).join(" "));
          _results.push(el.on(evt, this[callback]));
        }
        return _results;
      }
    };

    Class.prototype.__getDefaultContainer = function() {
      var container;
      container = this.el.find("[data-children-container]");
      if (container.length) {
        return container;
      } else {
        return this.el;
      }
    };

    Class.prototype.__getChildContainer__ = function(child_container) {
      return this.el.find("[data-children-container=" + child_container + "]");
    };

    return Class;

  })(World.Item);

  Atomic.Loader = (function() {
    var createStructure, loadJson, loadStructure, _ajax;
    _ajax = function(url, callback) {
      return Atomic.DOM.ajax({
        url: url,
        dataType: "json",
        error: function() {
          throw "Error loading app strcture json in " + url;
        },
        success: callback
      });
    };
    loadJson = function(url, callback) {
      var app;
      app = {};
      return _ajax(url, function(appJson) {
        var attr, value;
        for (attr in appJson) {
          value = appJson[attr];
          if (attr !== "structure") {
            app[attr] = value;
          }
        }
        app.structure = loadStructure(appJson.structure);
        return callback.call(callback, app);
      });
    };
    loadStructure = function(items, parent, callback) {
      var item, structure, _i, _len;
      structure = [];
      for (_i = 0, _len = items.length; _i < _len; _i++) {
        item = items[_i];
        structure.push(createStructure(item, parent));
      }
      return structure;
    };
    createStructure = function(item, parent) {
      var child, instance, _i, _len, _ref;
      instance = new App[item.type][item.name](item.attributes, [], item.options);
      if (parent) {
        parent.appendChild(instance);
      }
      if (item.children) {
        _ref = item.children;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          createStructure(child, instance);
        }
      }
      return instance;
    };
    return {
      loadJson: loadJson,
      createStructure: createStructure
    };
  })();

  App = (function() {
    var init;
    init = function(json_url, callback) {
      return Atomic.Loader.loadJson(json_url, function(app) {
        var app_container, component, _i, _len, _ref;
        App.Cache.init(app.structure);
        app_container = Atomic.DOM(document.body);
        _ref = app.structure;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          component = _ref[_i];
          component.render(app_container);
        }
        if (callback != null) {
          callback.call(app);
        }
        if (app.startRoute) {
          return App.Router.init(app.startRoute, app.startAnimation);
        }
      });
    };
    return {
      init: init,
      Base: Atomic.Class,
      Atom: {},
      Molecule: {},
      Organism: {},
      Template: {}
    };
  })();

  App.Constant = (function() {
    var animationFinishedEvents, cssTransform, isTouchScreen, transitionFinishedEvents, _height, _width;
    _width = window.innerWidth;
    _height = window.innerHeight;
    cssTransform = {
      'WebkitTransform': '-webkit-transform',
      'MozTransform': '-moz-transform',
      'OTransform': '-o-transform',
      'msTransform': '-ms-transform',
      'transform': 'transform'
    };
    transitionFinishedEvents = {
      'WebkitTransition': 'webkitTransitionEnd',
      'MozTransition': 'transitionend',
      'OTransition': 'oTransitionEnd',
      'msTransition': 'msTransitionEnd',
      'transition': 'transitionEnd'
    };
    animationFinishedEvents = {
      'WebkitAnimation': 'webkitAnimationEnd',
      'MozAnimation': 'animationend',
      'OAnimation': 'oAnimationEnd',
      'msAnimation': 'msAnimationEnd',
      'animation': 'animationEnd'
    };
    isTouchScreen = Modernizr.touch;
    return {
      TOUCH: isTouchScreen,
      DEVICE: {
        orientation: _width > _height ? 'portait' : 'landscape',
        width: window.innerWidth,
        height: window.innerHeight
      },
      EVENT: {
        animationEnd: animationFinishedEvents[Modernizr.prefixed("Animation")],
        transitionEnd: transitionFinishedEvents[Modernizr.prefixed("Transition")],
        touchStart: isTouchScreen ? "touchstart" : "mousedown",
        touchMove: isTouchScreen ? "touchmove" : "mousemove",
        touchEnd: isTouchScreen ? "touchend" : "mouseup"
      },
      CSS: {
        transform: cssTransform[Modernizr.prefixed("transform")]
      }
    };
  })();

  App.Gesture = (function() {
    var STARTED, asyncTrigger, register, trigger, _callbacks, _events, _gesture, _getFingersPositions, _getTouches, _handleEvent, _initialize, _lastEvent, _setDeltaPositions, _touchcancel, _touchend, _touchmove, _touchstart;
    STARTED = false;
    _events = {};
    _callbacks = [];
    _lastEvent = null;
    _gesture = {};
    _touchstart = function(event) {
      _initialize(event);
      return _handleEvent(event, "start");
    };
    _touchmove = function(event) {
      if (!_gesture.startPositions) {
        return;
      }
      _gesture.positions = _getFingersPositions(event);
      _gesture.deltaTime = new Date() - _gesture.startTime;
      _setDeltaPositions();
      return _handleEvent(event, "move");
    };
    _touchend = function(event) {
      if (!_gesture.startPositions) {
        return;
      }
      _gesture.deltaTime = new Date() - _gesture.startTime;
      _handleEvent(event, "end");
      return _initialize();
    };
    _touchcancel = function(event) {
      _handleEvent(event, "cancel");
      return _initialize();
    };
    _initialize = function(evt) {
      _lastEvent = null;
      _gesture = {};
      if (evt == null) {
        return;
      }
      _gesture.startTime = new Date();
      return _gesture.startPositions = _getFingersPositions(evt);
    };
    _getFingersPositions = function(evt) {
      var positions, touch, _i, _len, _ref;
      positions = [];
      _ref = _getTouches(evt);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        touch = _ref[_i];
        positions.push({
          x: touch.pageX,
          y: touch.pageY
        });
      }
      return positions;
    };
    _getTouches = function(evt) {
      if (evt.touches != null) {
        return evt.touches;
      } else {
        return [
          {
            pageX: evt.pageX,
            pageY: evt.pageY
          }
        ];
      }
    };
    _setDeltaPositions = function() {
      var i, position, _i, _len, _ref;
      i = 0;
      _gesture.deltas = [];
      _ref = _gesture.positions;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        position = _ref[_i];
        _gesture.deltas.push({
          x: position.x - _gesture.startPositions[i].x,
          y: position.y - _gesture.startPositions[i++].y
        });
      }
      return _gesture.deltaTime = (new Date()) - _gesture.startTime;
    };
    _handleEvent = function(event, moment) {
      var cb, _gestureClone, _i, _len, _results;
      _lastEvent = event;
      _gestureClone = JSON.parse(JSON.stringify(_gesture));
      _results = [];
      for (_i = 0, _len = _callbacks.length; _i < _len; _i++) {
        cb = _callbacks[_i];
        if (!!cb[moment]) {
          _results.push(cb[moment].call(null, event, _gestureClone));
        }
      }
      return _results;
    };
    document.body.addEventListener(App.Constant.EVENT.touchStart, _touchstart);
    document.body.addEventListener(App.Constant.EVENT.touchMove, _touchmove);
    document.body.addEventListener(App.Constant.EVENT.touchEnd, _touchend);
    document.body.addEventListener("touchcancel", _touchcancel);
    register = function(events, callbacks) {
      var evt, _i, _len, _results;
      _callbacks.push(callbacks);
      _results = [];
      for (_i = 0, _len = events.length; _i < _len; _i++) {
        evt = events[_i];
        _results.push(_events[evt] = new CustomEvent(evt, {
          bubbles: true,
          cancelable: true
        }));
      }
      return _results;
    };
    trigger = function(event_name, data) {
      var evt, key;
      evt = _events[event_name];
      evt.originalEvent = _lastEvent;
      evt.gestureData = JSON.parse(JSON.stringify(_gesture));
      for (key in data) {
        if (data) {
          evt.gestureData[key] = data[key];
        }
      }
      return _lastEvent.target.dispatchEvent(evt);
    };
    asyncTrigger = function(event_name, target, data) {
      return target.dispatchEvent(_events[event_name], data);
    };
    return {
      register: register,
      trigger: trigger,
      asyncTrigger: asyncTrigger
    };
  })();

  App.Gesture.register(["tap", "hold", "doubleTap", "singleTap"], (function() {
    var DOUBLE_TIME, GAP, HOLD_TIME, TAP_TIME, end, move, start, _holdTimeout, _lazyHold, _lazySimpleTap, _possibleDouble, _possibleTap, _simpleTimeout;
    HOLD_TIME = 300;
    TAP_TIME = 300;
    DOUBLE_TIME = 450;
    GAP = 3;
    _possibleTap = false;
    _possibleDouble = false;
    _holdTimeout = null;
    _simpleTimeout = null;
    start = function(event, gd) {
      _possibleTap = gd.startPositions.length === 1;
      if (_possibleTap) {
        _holdTimeout = setTimeout(_lazyHold(event.target), HOLD_TIME);
      }
    };
    move = function(event, gd) {
      var delta;
      delta = gd.deltas[0];
      _possibleTap = gd.deltas.length === 1 && Math.abs(delta.x) < GAP && Math.abs(delta.y) < GAP;
      if (!_possibleTap) {
        clearTimeout(_holdTimeout);
        clearTimeout(_simpleTimeout);
      }
    };
    end = function(event, gd) {
      if (_possibleTap && gd.deltaTime < TAP_TIME) {
        clearTimeout(_holdTimeout);
        App.Gesture.trigger("tap");
        if (_possibleDouble) {
          clearTimeout(_simpleTimeout);
          App.Gesture.trigger("doubleTap");
          _possibleDouble = false;
        } else {
          _simpleTimeout = setTimeout(_lazySimpleTap(event.target), DOUBLE_TIME);
          _possibleDouble = true;
        }
      }
    };
    _lazyHold = function(target) {
      return function() {
        return App.Gesture.asyncTrigger("hold", target);
      };
    };
    _lazySimpleTap = function(target) {
      return function() {
        App.Gesture.asyncTrigger("singleTap", target);
        return _possibleDouble = false;
      };
    };
    return {
      start: start,
      move: move,
      end: end
    };
  })());

  App.Gesture.register(["swiping", "swipingHorizontal", "swipingVertical", "swipe"], (function() {
    var _first, _log, _swiping, _swipingH, _swipingV;
    _first = true;
    _swiping = false;
    _swipingH = false;
    _swipingV = false;
    _log = function(txt, clear) {
      if (clear) {
        return $("#page1 header h1").text(txt);
      } else {
        return $("#page1 header h1").text($("#page1 header h1").text() + "; " + txt);
      }
    };
    return {
      start: function() {
        _first = true;
        _swiping = false;
        _swipingH = false;
        _swipingV = false;
        return true;
      },
      move: function(event, gd) {
        if (gd.deltas.length === 1) {
          if (!_swiping) {
            _swipingH = Math.abs(gd.deltas[0].x / gd.deltas[0].y) >= 2;
            _swipingV = Math.abs(gd.deltas[0].y / gd.deltas[0].x) >= 2;
          }
          App.Gesture.trigger("swiping", {
            first: _first
          });
          if (_swipingH) {
            App.Gesture.trigger("swipingHorizontal", {
              first: _first
            });
          }
          if (_swipingV) {
            App.Gesture.trigger("swipingVertical", {
              first: _first
            });
          }
          _swiping = true;
          _first = false;
        }
        return true;
      },
      end: function(event, gd) {
        if (_swiping) {
          App.Gesture.trigger("swipe");
        }
        return true;
      }
    };
  })());

  Commons = Commons || {};

  Commons.data = function() {
    var key, value, _ref, _results;
    if (this.attributes.data) {
      _ref = this.attributes.data;
      _results = [];
      for (key in _ref) {
        value = _ref[key];
        _results.push(this.el.attr("data-" + key, value));
      }
      return _results;
    }
  };

  Commons = Commons || {};

  Commons.router = function() {
    var parts, _ref;
    if ((_ref = this.options) != null ? _ref.router : void 0) {
      parts = this.options.router.split(":");
      return this.el.on("tap", (function(_this) {
        return function(ev) {
          App.Router[parts[0]](parts[1]);
          ev.originalEvent.preventDefault();
          return ev.originalEvent.stopPropagation();
        };
      })(this));
    }
  };

  App.Atom.Button = (function(_super) {
    __extends(Button, _super);

    Button.type = "Button";

    Button.template = "<button class=\"{{class}}\">\n    <span class=\"icon {{icon}}\"></span>\n    {{text}}\n</button>";

    function Button() {
      Button.__super__.constructor.apply(this, arguments);
      Commons.router.call(this);
      Commons.data.call(this);
    }

    return Button;

  })(Atomic.Class);

  App.Atom.Input = (function(_super) {
    __extends(Input, _super);

    function Input() {
      return Input.__super__.constructor.apply(this, arguments);
    }

    Input.type = "Input";

    Input.template = "<input type=\"{{type}}\" placeholder=\"{{placeholder}}\" class=\"{{class}}\" name=\"{{name}}\" />";

    Input.prototype.value = function() {
      return this.el.val();
    };

    return Input;

  })(Atomic.Class);

  App.Atom.Label = (function(_super) {
    __extends(Label, _super);

    function Label() {
      return Label.__super__.constructor.apply(this, arguments);
    }

    Label.type = "Label";

    Label.template = "<label class=\"{{class}}\">{{text}}</label>";

    return Label;

  })(Atomic.Class);

  App.Atom.Li = (function(_super) {
    __extends(Li, _super);

    Li.type = "Li";

    Li.template = "<li>\n	{{text}}\n	<small>{{description}}</small>\n</li>";

    function Li() {
      Li.__super__.constructor.apply(this, arguments);
      Commons.router.call(this);
    }

    return Li;

  })(Atomic.Class);

  App.Atom.Loading = (function(_super) {
    __extends(Loading, _super);

    function Loading() {
      return Loading.__super__.constructor.apply(this, arguments);
    }

    Loading.type = "Loading";

    Loading.template = "<div class=\"atom_loading\">\n	<div class=\"\">{{text}}</div>\n	<span class=\"icon looping\"></span>\n</div>";

    return Loading;

  })(Atomic.Class);

  App.Atom.P = (function(_super) {
    __extends(P, _super);

    function P() {
      return P.__super__.constructor.apply(this, arguments);
    }

    P.type = "Label";

    P.template = "<p class=\"{{class}}\">{{text}}</p>";

    return P;

  })(Atomic.Class);

  App.Atom.Select = (function(_super) {
    __extends(Select, _super);

    Select.type = "Select";

    Select.template = "<select name=\"{{name}}\" class=\"{{class}}\" id=\"{{id}}\">\n    {{#options}}\n        <option value=\"{{value}}\">{{label}}</option>\n    {{/options}}\n</select>";

    function Select() {
      Select.__super__.constructor.apply(this, arguments);
      if (this.attributes.value) {
        this.el.val(attributes.value);
      }
    }

    Select.prototype.value = function() {
      return this.el.val();
    };

    return Select;

  })(Atomic.Class);

  App.Atom.Textarea = (function(_super) {
    __extends(Textarea, _super);

    function Textarea() {
      return Textarea.__super__.constructor.apply(this, arguments);
    }

    Textarea.type = "Textarea";

    Textarea.template = "<textarea class=\"{{class}}\"></textarea>";

    Textarea.prototype.value = function() {
      return this.el.val();
    };

    return Textarea;

  })(Atomic.Class);

  App.Atom.Title = (function(_super) {
    __extends(Title, _super);

    Title.type = "Title";

    Title.template = "<h{{size}} class=\"{{class}}\">{{text}}</h{{size}}>";

    function Title() {
      Title.__super__.constructor.apply(this, arguments);
    }

    return Title;

  })(Atomic.Class);

  App.Molecule.Footer = (function(_super) {
    __extends(Footer, _super);

    function Footer() {
      return Footer.__super__.constructor.apply(this, arguments);
    }

    Footer.type = "Footer";

    Footer.template = "<footer>\n    <nav data-children-container=\"navigation\"></nav>\n</footer>";

    Footer.prototype.render = function() {
      Footer.__super__.render.apply(this, arguments);
      return this.el[0].addEventListener("touchmove", function(e) {
        return e.preventDefault();
      });
    };

    return Footer;

  })(Atomic.Class);

  App.Molecule.Form = (function(_super) {
    __extends(Form, _super);

    Form.type = "Form";

    Form.template = "<form class=\"{{class}}\" id=\"{{id}}\" onsubmit=\"return false;\">\n  <h1>{{title}}</h1>\n</form>";

    function Form() {
      var submit_attributes;
      Form.__super__.constructor.apply(this, arguments);
      submit_attributes = {
        text: "Submit",
        "class": "big fluid accept"
      };
      this.submit = new App.Atom.Button(submit_attributes);
      this.appendChild(this.submit);
      this.submit.el.bind("tap", ((function(_this) {
        return function() {
          return console.log(_this.value());
        };
      })(this)));
    }

    Form.prototype.onSubmit = function(callback) {
      return this.submit.el.bind("tap", ((function(_this) {
        return function() {
          return callback.call(_this, _this.value());
        };
      })(this)));
    };

    Form.prototype.value = function() {
      var child, values, _i, _len, _ref;
      values = {};
      _ref = this.children;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        if (child.value != null) {
          values[child.attributes.name] = child.value();
        }
      }
      return values;
    };

    return Form;

  })(Atomic.Class);

  App.Molecule.Header = (function(_super) {
    __extends(Header, _super);

    function Header() {
      return Header.__super__.constructor.apply(this, arguments);
    }

    Header.type = "Header";

    Header.template = "<header>\n    <h1>{{title}}</h1>\n    <nav class=\"left\" data-children-container=\"left\"></nav>\n    <nav class=\"right\" data-children-container=\"right\"></nav>\n</header>";

    Header.prototype.render = function() {
      var button, buttons, _i, _len, _results;
      Header.__super__.render.apply(this, arguments);
      this.el[0].addEventListener("touchmove", function(e) {
        return e.preventDefault();
      });
      buttons = this.children.filter(function(item) {
        return item.namespace === "Button";
      });
      _results = [];
      for (_i = 0, _len = buttons.length; _i < _len; _i++) {
        button = buttons[_i];
        if (button.visibleOnSection) {
          button.el.attr("data-show-section", button.visibleOnSection);
        }
        if (button.hiddenOnSection) {
          _results.push(button.el.attr("data-hide-section", button.hiddenOnSection));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    return Header;

  })(Atomic.Class);

  App.Molecule.List = (function(_super) {
    __extends(List, _super);

    function List() {
      return List.__super__.constructor.apply(this, arguments);
    }

    List.type = "List";

    List.template = "<ul class=\"{{class}}\"></ul>";

    return List;

  })(Atomic.Class);

  App.Molecule.Nav = (function(_super) {
    __extends(Nav, _super);

    function Nav() {
      return Nav.__super__.constructor.apply(this, arguments);
    }

    Nav.type = "Nav";

    Nav.template = "<nav class=\"{{class}}\"></nav>";

    return Nav;

  })(Atomic.Class);

  App.Molecule.Search = (function(_super) {
    __extends(Search, _super);

    Search.template = "";

    function Search(attributes, children, options) {
      this.attributes = attributes != null ? attributes : {};
      this.children = children != null ? children : [];
      this.options = options != null ? options : {};
      Search.__super__.constructor.apply(this, arguments);
      console.log("Vaaa", this);
    }

    return Search;

  })(Atomic.Class);

  App.Organism.Aside = (function(_super) {
    __extends(Aside, _super);

    function Aside() {
      return Aside.__super__.constructor.apply(this, arguments);
    }

    Aside.type = "Aside";

    Aside.template = "<aside class=\"{{class}}\" id=\"{{id}}\"></aside>";

    Aside.prototype.render = function() {
      var child, _i, _len, _ref, _results;
      Aside.__super__.render.apply(this, arguments);
      this.sections = {};
      _ref = this.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        if (child.constructor.name === "Section") {
          _results.push(this.sections[child.attributes.id] = child);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    Aside.prototype.activate = function() {
      return this.el.addClass("active");
    };

    Aside.prototype.deactivate = function() {
      return this.el.removeClass("active");
    };

    return Aside;

  })(Atomic.Class);

  App.Organism.Modal = (function(_super) {
    __extends(Modal, _super);

    function Modal() {
      return Modal.__super__.constructor.apply(this, arguments);
    }

    Modal.type = "Modal";

    Modal.template = "<div id=\"{{id}}\" data-modal=\"{{animation}}\" class=\"{{class}}\"></div>";

    return Modal;

  })(Atomic.Class);

  App.Organism.Section = (function(_super) {
    __extends(Section, _super);

    function Section() {
      this._pulling = __bind(this._pulling, this);
      return Section.__super__.constructor.apply(this, arguments);
    }

    Section.PULL_GAP = 70;

    Section.type = "Section";

    Section.template = "<section id=\"{{id}}\" class=\"{{class}}\"></section>";

    Section._onTransitionEnd = function(pullLayer) {
      return (function(_this) {
        return function(ev) {
          var section;
          section = $(ev.currentTarget);
          if (section.attr("data-state") === "pull-out") {
            return pullLayer.removeClass("visible");
          }
        };
      })(this);
    };

    Section.prototype.swipped = 0;

    Section.prototype.pulled = false;

    Section.prototype._createPullLayer = function(parent) {
      this.pullLayer = $("<div class=\"pullrefresh\">\n  <p class=\"font big centered margins\">Pull & Refresh</p>\n  <p class=\"font centered\"><span class=\"icon looping\"></span></p>\n</div>");
      return parent.append(this.pullLayer);
    };

    Section.prototype._pulling = function(ev) {
      var delta, diff;
      delta = ev.gestureData.deltas[0].y;
      if (!this.pulled) {
        if (ev.currentTarget.scrollTop === 0) {
          diff = Math.max(delta, 0);
          if (diff > 0) {
            this.pullLayer.addClass("visible");
            this.el.removeAttr("data-state");
            this.swipped = diff * 0.35;
            this.el.css(App.Constant.CSS.transform, "translateY(" + this.swipped + "px)");
            ev.originalEvent.preventDefault();
          }
        }
      } else {
        ev.originalEvent.preventDefault();
      }
      return true;
    };

    Section.prototype.pull = function(show) {
      if (show == null) {
        show = true;
      }
      this.el.removeAttr("style");
      this.pulled = show;
      if (show) {
        this.el.attr("data-state", "pull-in");
        return this.trigger("pull");
      } else {
        this.el.attr("data-state", "pull-out");
        return this.trigger("unpull");
      }
    };

    Section.prototype.render = function() {
      var onEnd;
      Section.__super__.render.apply(this, arguments);
      if (this.options.pullable) {
        this._createPullLayer(this.parent.el);
        onEnd = this.constructor._onTransitionEnd(this.pullLayer);
        this.el.on(App.Constant.EVENT.transitionEnd, onEnd);
        this.el.on("swipingVertical", this._pulling);
        return this.el.on("swipe", (function(_this) {
          return function(ev) {
            if (_this.swipped > 0) {
              _this.pull(_this.swipped > _this.constructor.PULL_GAP);
              _this.swipped = 0;
            }
            return true;
          };
        })(this));
      }
    };

    return Section;

  })(Atomic.Class);

  App.Template.Article = (function(_super) {
    __extends(Article, _super);

    Article.type = "Article";

    Article.template = "<article id=\"{{id}}\" class=\"{{class}}\"></article>";

    Article._animations_in_progress = 0;

    Article._startRouterAnimation = function(element, animation, direction) {
      Article._animations_in_progress += 1;
      element.addClass("active").attr("data-animation", animation);
      return element.attr("data-direction", direction);
    };

    Article._onAnimationEnd = function(ev) {
      var article, el, _ref;
      el = Atomic.DOM(ev.currentTarget);
      article = App.Cache.article(el.attr("id"));
      if ((_ref = el.attr("data-direction")) === "out" || _ref === "back-out") {
        el.removeClass("active");
        article.trigger("unload");
      } else {
        article.trigger("load");
        if (article.aside) {
          App.Cache.aside(article.aside).el.addClass("active");
        }
      }
      el.removeAttr("data-direction").removeAttr("data-animation").removeAttr("data-start-animation");
      return setTimeout((function() {
        return Article._animations_in_progress--;
      }), 50);
    };

    Article.ASIDE_WIDTH = 250;

    Article._swiped_px = 0;

    Article._aside_open = false;

    Article._aside_transforming = false;

    Article._aside_callback = null;

    Article._onSwipingHorizontal = function(ev) {
      var delta;
      if (Article._aside_open) {
        return ev.preventDefault();
      } else {
        delta = Math.max(ev.gestureData.deltas[0].x, 0);
        if (delta > Article.ASIDE_WIDTH) {
          delta = ((delta - Article.ASIDE_WIDTH) * 0.2) + Article.ASIDE_WIDTH;
        }
        Article._swiped_px = delta;
        Atomic.DOM(ev.currentTarget).css(App.Constant.CSS.transform, "translateX(" + delta + "px)");
        return ev.originalEvent.preventDefault();
      }
    };

    Article._onSwipe = function(ev) {
      var article;
      article = Atomic.DOM(ev.currentTarget);
      if ((!Article._aside_open && Article._swiped_px > 100) || Article._aside_open) {
        App.Cache.article().toggleAside();
      } else if (Article._swiped_px > 0) {
        article.on(App.Constant.EVENT.transitionEnd, Article._onTransitionEnd);
        article.addClass("aside");
      }
      article.removeAttr("style");
      return Article._swiped_px = 0;
    };

    Article._onTransitionEnd = function(ev) {
      var target;
      target = Atomic.DOM(ev.currentTarget);
      target.off(App.Constant.EVENT.transitionEnd, Article._onTransitionEnd);
      if (target.hasClass("aside") && !target.attr("data-aside")) {
        target.removeClass("aside");
        Article._aside_open = false;
      } else {
        Article._aside_open = true;
      }
      Article._aside_transforming = false;
      if (Article._aside_callback) {
        Article._aside_callback.call(Article);
        return Article._aside_callback = null;
      }
    };

    Article._onTap = function() {
      if (Article._aside_open && !Article._aside_transforming) {
        return App.Cache.article().toggleAside();
      }
    };

    function Article(attributes, children, options) {
      if (attributes == null) {
        attributes = {};
      }
      if (children == null) {
        children = [];
      }
      if (options == null) {
        options = {};
      }
      Article.__super__.constructor.apply(this, arguments);
      this.el.on(App.Constant.EVENT.animationEnd, this.constructor._onAnimationEnd);
      this.animation = options.animation;
      this.aside = options.aside;
      if (this.aside) {
        this.el.on("touchend", this.constructor._onTap);
        this.el.on("swipingHorizontal", this.constructor._onSwipingHorizontal);
        this.el.on("swipe", this.constructor._onSwipe);
      }
    }

    Article.prototype.render = function() {
      var child, _i, _len, _ref, _results;
      Article.__super__.render.apply(this, arguments);
      this.sections = {};
      _ref = this.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        if (child.constructor.type === "Section") {
          _results.push(this.sections[child.attributes.id] = child);
        }
      }
      return _results;
    };

    Article.prototype.show = function(is_back, start_animation) {
      var current, remoteArgs, thisArgs;
      if (is_back == null) {
        is_back = false;
      }
      if (start_animation == null) {
        start_animation = "fade";
      }
      current = App.Cache.article();
      this.trigger("beforeload");
      App.Cache.setArticle(this);
      if (this.aside) {
        App.Cache.aside(this.aside).deactivate();
      }
      if (current != null ? current.aside : void 0) {
        App.Cache.aside(current.aside).deactivate();
      }
      this.el.addClass("active");
      if (!current) {
        this.el.attr("data-start-animation", "start_" + start_animation);
        return;
      }
      current.trigger("beforeunload");
      thisArgs = is_back ? [this.el, current.animation, "back-in"] : [this.el, this.animation, "in"];
      remoteArgs = is_back ? [current.el, current.animation, "back-out"] : [current.el, this.animation, "out"];
      this.constructor._startRouterAnimation.apply(null, thisArgs);
      return this.constructor._startRouterAnimation.apply(null, remoteArgs);
    };

    Article.prototype.section = function(id) {
      var sections;
      sections = this.el.children("section");
      sections.removeClass("active").filter("#" + id).addClass("active");
      this.trigger("load:" + id);
      if (this.constructor._aside_open) {
        return this.toggleAside();
      }
    };

    Article.prototype.toggleAside = function(callback) {
      if (this.constructor._aside_transforming === true) {
        return false;
      }
      this.constructor._aside_callback = callback;
      this.constructor._aside_transforming = true;
      App.Cache.aside(this.aside).el.addClass("active");
      this.el.on(App.Constant.EVENT.transitionEnd, this.constructor._onTransitionEnd);
      if (this.el.attr("data-aside")) {
        this.constructor._aside_open = false;
        return this.el.addClass("aside").removeAttr("data-aside");
      } else {
        this.constructor._aside_open = true;
        return this.el.addClass("aside").attr("data-aside", "show");
      }
    };

    return Article;

  })(Atomic.Class);

  App.Cache = (function() {
    var init, setArticle, _cache, _cacheCommons;
    _cache = {};
    init = function(app) {
      return _cacheCommons(app);
    };
    setArticle = function(instance) {
      return _cache.currentArticle = instance;
    };
    _cacheCommons = function(app) {
      var component, node_name, _i, _len, _ref, _results;
      _results = [];
      for (_i = 0, _len = app.length; _i < _len; _i++) {
        component = app[_i];
        if (!((_ref = component.constructor.type) === "Article" || _ref === "Aside")) {
          continue;
        }
        node_name = component.constructor.type.toLowerCase();
        _cache[node_name] = _cache[node_name] || {};
        _results.push(_cache[node_name][component.attributes.id] = component);
      }
      return _results;
    };
    return {
      init: init,
      setArticle: setArticle,
      aside: function(id) {
        return _cache.aside[id];
      },
      article: function(id) {
        if (id) {
          return _cache.article[id];
        } else {
          return _cache.currentArticle;
        }
      },
      register: function(namespace, item) {
        _cache[namespace] = _cache[namespace] || {};
        return _cache[namespace][item.attributes.id] = item;
      }
    };
  })();

  App.Modal = (function() {
    var hide, show, _blockDiv, _current, _onAnimationEnd;
    _current = null;
    _blockDiv = document.createElement("div");
    _blockDiv.setAttribute("data-block-modal", "default");
    document.body.appendChild(_blockDiv);
    _onAnimationEnd = function(ev) {
      var target;
      target = Atomic.DOM(ev.currentTarget);
      if (target.attr("data-direction") === "in") {
        target.attr("data-status", "active");
      } else {
        target.removeAttr("data-status");
      }
      target.removeAttr("data-direction");
      return target.off(App.Constant.EVENT.animationEnd, _onAnimationEnd);
    };
    show = function(id) {
      _current = Atomic.DOM(document.getElementById(id));
      if (_current.length === 1) {
        _blockDiv.className = "active";
        _current.on(App.Constant.EVENT.animationEnd, _onAnimationEnd);
        return _current.attr("data-direction", "in");
      }
    };
    hide = function() {
      if (_current) {
        _blockDiv.removeAttribute("class");
        _current.on(App.Constant.EVENT.animationEnd, _onAnimationEnd);
        _current.attr("data-direction", "out");
      }
      return _current = null;
    };
    return {
      show: show,
      hide: hide
    };
  })();

  App.Router = (function() {
    var article, aside, back, init, modal, section, _routes, _showArticle;
    _routes = [];
    _showArticle = function(id, is_back, start_animation) {
      return function() {
        var instance;
        instance = App.Cache.article(id);
        console.log(instance);
        instance.show(is_back, start_animation);
        if (is_back) {
          return _routes.length--;
        } else {
          return _routes.push(id);
        }
      };
    };
    init = function(article_id, start_animation) {
      return article(article_id, false, start_animation);
    };
    article = function(id, is_back, start_animation) {
      var funct;
      if (is_back == null) {
        is_back = false;
      }
      if (!(App.Template.Article._animations_in_progress > 0)) {
        App.Modal.hide();
        funct = _showArticle(id, is_back, start_animation);
        if (App.Template.Article._aside_open) {
          return App.Cache.article().toggleAside(funct);
        } else {
          return funct();
        }
      }
    };
    section = function(id) {
      App.Modal.hide();
      return App.Cache.article().section(id);
    };
    modal = function(id) {
      if (id === "hide") {
        return App.Modal.hide();
      } else {
        return App.Modal.show(id);
      }
    };
    aside = function() {
      return App.Cache.article().toggleAside();
    };
    back = function() {
      var len;
      len = _routes.length;
      if (len <= 1) {
        return false;
      }
      return article(_routes[len - 2], true);
    };
    return {
      init: init,
      article: article,
      section: section,
      modal: modal,
      back: back,
      aside: aside
    };
  })();

  this.App = App;

}).call(this);

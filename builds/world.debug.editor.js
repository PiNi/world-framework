(function() {
  var Editor;

  window.Editor = Editor = {};

}).call(this);

(function() {
  Editor.Constants = {
    DATA_ATTRIBUTES: {
      NAME: "data-component-name",
      TYPE: "data-component-type",
      UID: "data-component-uid"
    },
    COMPONENTS: {
      "Article": {
        CONTAINERS: ["__root__"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: ["animation", "aside"]
      },
      "Aside": {
        CONTAINERS: ["__root__"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Modal": {
        CONTAINERS: ["__root__"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Section": {
        CONTAINERS: ["Article", "Aside"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: ["pullable"]
      },
      "Header": {
        CONTAINERS: ["Article", "Aside"],
        ATTRIBUTES: ["id", "class", "title"],
        OPTIONS: []
      },
      "Form": {
        CONTAINERS: ["Section"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "List": {
        CONTAINERS: ["Section"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Nav": {
        CONTAINERS: ["Section", "Header", "Footer"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Search": {
        CONTAINERS: ["Section", "Header", "Footer"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Footer": {
        CONTAINERS: ["Article", "Aside"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Button": {
        CONTAINERS: ["Section", "Form", "Nav", "Header", "Footer"],
        ATTRIBUTES: ["id", "class", "text", "icon", "container"],
        OPTIONS: ["router"]
      },
      "Input": {
        CONTAINERS: ["Section", "Form"],
        ATTRIBUTES: ["id", "class", "placeholder", "value"],
        OPTIONS: []
      },
      "Label": {
        CONTAINERS: ["Section", "Form"],
        ATTRIBUTES: ["id", "class", "text"],
        OPTIONS: []
      },
      "Li": {
        CONTAINERS: ["List"],
        ATTRIBUTES: ["id", "class", "text", "description"],
        OPTIONS: ["router"]
      },
      "Loading": {
        CONTAINERS: ["Section"],
        ATTRIBUTES: ["id", "class", "text"],
        OPTIONS: []
      },
      "P": {
        CONTAINERS: ["Section", "Form"],
        ATTRIBUTES: ["id", "class", "text"],
        OPTIONS: []
      },
      "Select": {
        CONTAINERS: ["Section", "Form"],
        ATTRIBUTES: ["id", "class"],
        OPTIONS: []
      },
      "Textarea": {
        CONTAINERS: ["Section", "Form"],
        ATTRIBUTES: ["id", "class", "placeholder", "value"],
        OPTIONS: []
      },
      "Title": {
        CONTAINERS: ["Section", "Form"],
        ATTRIBUTES: ["id", "class", "text"],
        OPTIONS: []
      }
    }
  };

}).call(this);

(function() {
  Editor.Components = (function() {
    var load, _bindElementEvents, _createElement, _dragEnd, _dragStart, _dragging_component;
    _dragging_component = null;
    _dragStart = function(component, app_namespace) {
      return function(evt) {
        return _dragging_component = {
          component: component,
          namespace: app_namespace
        };
      };
    };
    _dragEnd = function(evt) {
      return _dragging_component = null;
    };
    _createElement = function(container, name) {
      var el;
      el = $("<li>" + name + "</li>").attr("draggable", "true");
      container.append(el);
      return el;
    };
    _bindElementEvents = function(el, component, app_namespace) {
      el.on("dragstart", _dragStart(component, app_namespace));
      return el.on("dragend", _dragEnd);
    };
    load = function(components_data) {
      var component, components, container, name, type, _results;
      _results = [];
      for (type in components_data) {
        components = components_data[type];
        container = $("#NS_CONTAINER_" + type);
        _results.push((function() {
          var _results1;
          _results1 = [];
          for (name in components) {
            component = components[name];
            _results1.push(_bindElementEvents(_createElement(container, name), component, type));
          }
          return _results1;
        })());
      }
      return _results;
    };
    return {
      load: load,
      getCurrent: function() {
        return _dragging_component;
      }
    };
  })();

}).call(this);

(function() {
  Editor.Canvas = (function(C) {
    var COMPONENT_RENDERER, appendComponent, regenerateTree, removeComponent, _closestParentSelector, _components, _componentsTree, _container, _getClosestAllowedContainer, _getNodeTree, _guid, _handleDrop, _lastDroppable, _onDraggingLeave, _onDraggingOver;
    COMPONENT_RENDERER = templayed("<div \n  " + C.DATA_ATTRIBUTES.NAME + "=\"{{name}}\" \n  " + C.DATA_ATTRIBUTES.TYPE + "=\"{{type}}\"\n  " + C.DATA_ATTRIBUTES.UID + "=\"{{uid}}\"\n  >\n  <span class=\"title\">{{name}}</span>\n  <span class=\"id\">{{attributes.id}}</span>\n</div>");
    _container = $("#canvas");
    _components = {};
    _componentsTree = [];
    _lastDroppable = null;
    _closestParentSelector = function(allowedParents) {
      var component_name, selector, _i, _len;
      selector = [];
      for (_i = 0, _len = allowedParents.length; _i < _len; _i++) {
        component_name = allowedParents[_i];
        selector.push("[" + C.DATA_ATTRIBUTES.NAME + "=\"" + component_name + "\"]");
      }
      return selector.join(",");
    };
    _getClosestAllowedContainer = function(el, name) {
      var allowedParents, ret, selector;
      ret = [];
      if (name) {
        allowedParents = C.COMPONENTS[name].CONTAINERS;
        if (allowedParents) {
          selector = _closestParentSelector(allowedParents);
          ret = el.closest(selector);
        }
      }
      return ret;
    };
    removeComponent = function(uid) {
      _components[uid].el.remove();
      delete _components[uid];
      return regenerateTree();
    };
    appendComponent = function(component_data, container) {
      var el, uid;
      uid = _guid();
      el = $(COMPONENT_RENDERER({
        name: component_data.component.name,
        type: component_data.namespace,
        uid: uid,
        attributes: {},
        options: {}
      }));
      container.append(el);
      _components[uid] = {
        uid: uid,
        name: component_data.component.name,
        type: component_data.namespace,
        attributes: component_data.component.attributes,
        options: component_data.component.options,
        el: el
      };
      return el;
    };
    _handleDrop = function(ev) {
      var comp_data, el;
      if (_lastDroppable.length) {
        comp_data = Editor.Components.getCurrent();
        el = appendComponent(comp_data, _lastDroppable);
        el.trigger("click");
        regenerateTree();
      }
      return _onDraggingLeave(false);
    };
    _onDraggingOver = function(evt) {
      var component_data;
      component_data = Editor.Components.getCurrent();
      _lastDroppable = _getClosestAllowedContainer($(evt.target), component_data.component.name);
      _lastDroppable.addClass("droppable");
      evt.preventDefault();
      return evt.stopPropagation();
    };
    _onDraggingLeave = function(evt) {
      if (_lastDroppable) {
        _lastDroppable.removeClass("droppable");
        _lastDroppable = null;
        if (evt) {
          evt.preventDefault();
          return evt.stopPropagation();
        }
      }
    };
    _guid = function() {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r, v;
        r = Math.random() * 16 | 0;
        v = c === 'x' ? r : r & 3 | 8;
        return v.toString(16);
      }).toUpperCase();
    };
    _getNodeTree = function(parent_node) {
      var items, node, _i, _len, _ref;
      items = [];
      _ref = parent_node.childNodes;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        if (node.nodeName === "DIV") {
          items.push({
            uid: node.getAttribute(C.DATA_ATTRIBUTES.UID),
            children: _getNodeTree(node)
          });
        }
      }
      return items;
    };
    regenerateTree = function() {
      _componentsTree = _getNodeTree(_container[0]);
      return true;
    };
    _container.attr(C.DATA_ATTRIBUTES.NAME, "__root__");
    _container.on("dragover", _onDraggingOver);
    _container.on("dragleave", _onDraggingLeave);
    _container.on("drop", _handleDrop);
    return {
      container: _container,
      appendComponent: appendComponent,
      removeComponent: removeComponent,
      regenerateTree: regenerateTree,
      getTree: function() {
        return _componentsTree;
      },
      getComponents: function() {
        return _components;
      },
      getComponent: function(uid) {
        return _components[uid];
      }
    };
  })(Editor.Constants);

}).call(this);

(function() {
  Editor.Loader = (function() {
    var getComponents, _components, _loaded;
    _loaded = false;
    _components = {
      "Template": {},
      "Organism": {},
      "Molecule": {},
      "Atom": {}
    };
    getComponents = function(force) {
      var component_name, compontent, ns, _ref;
      if (force == null) {
        force = false;
      }
      if (_loaded && !force) {
        return _components;
      }
      for (ns in _components) {
        _components[ns] = {};
        _ref = App[ns];
        for (component_name in _ref) {
          compontent = _ref[component_name];
          _components[ns][component_name] = compontent;
        }
      }
      return _components;
    };
    return {
      getComponents: getComponents
    };
  })();

}).call(this);

(function() {
  Editor.Properties = (function(C) {
    var ELEMENTS, RENDERER, select, unselect, _component, _mixObjectProperties, _onDelete, _onKeyUp, _paintProperties, _setHeaderTitle;
    RENDERER = templayed("<li>\n  <div class=\"label\">{{label}}</div>\n  <div class=\"value\">\n    <input type=\"text\" data-key=\"{{label}}\" data-value-type=\"{{type}}\" value=\"{{value}}\" />\n  </div>\n</li>");
    ELEMENTS = {
      header: $("#aside_properties > header"),
      section: $("#aside_properties > section"),
      footer: $("#aside_properties > footer"),
      attributes: $("#aside_properties > section ul#attributes"),
      options: $("#aside_properties > section ul#options"),
      btn_delete: $("#aside_properties > footer button")
    };
    _component = null;
    _mixObjectProperties = function(default_obj, obj) {
      var key, _i, _len;
      if (obj == null) {
        obj = {};
      }
      for (_i = 0, _len = default_obj.length; _i < _len; _i++) {
        key = default_obj[_i];
        obj[key] || (obj[key] = "");
      }
      return obj;
    };
    _paintProperties = function(type) {
      var container, key, value, _ref, _results;
      container = ELEMENTS[type];
      container.find(":not(.title)").remove();
      if (_component) {
        _ref = _component[type];
        _results = [];
        for (key in _ref) {
          value = _ref[key];
          _results.push(container.append($(RENDERER({
            type: type,
            label: key,
            value: value
          }))));
        }
        return _results;
      }
    };
    _onKeyUp = function(evt) {
      var key, target, type, value;
      target = evt.currentTarget;
      type = target.getAttribute("data-value-type");
      key = target.getAttribute("data-key");
      value = target.value.trim();
      return _component[type][key] = value;
    };
    _onDelete = function(evt) {
      if (_component) {
        Editor.Canvas.removeComponent(_component.uid);
        return unselect();
      }
    };
    _setHeaderTitle = function() {
      ELEMENTS.header.attr("data-component-type", _component.type);
      ELEMENTS.section.attr("data-component-type", _component.type);
      return ELEMENTS.header.html(_component.type + " :: " + _component.name);
    };
    unselect = function() {
      _component = null;
      ELEMENTS.btn_delete.attr("disabled", "disabled");
      _paintProperties("attributes");
      return _paintProperties("options");
    };
    select = function(evt, component) {
      var attributes, default_attributes, default_options, options, uid;
      if (evt) {
        $(evt.currentTarget).addClass("selected");
        uid = evt.currentTarget.getAttribute(C.DATA_ATTRIBUTES.UID);
        component = Editor.Canvas.getComponent(uid);
        attributes = component.attributes;
        options = component.options;
        evt.stopPropagation();
        evt.preventDefault();
      }
      if (_component) {
        if (_component.uid !== component.uid) {
          _component.el.removeClass("selected");
        }
      }
      _component = component;
      if ((attributes == null) && (options == null)) {
        default_attributes = C.COMPONENTS[component.name].ATTRIBUTES;
        default_options = C.COMPONENTS[component.name].OPTIONS;
        component.attributes = _mixObjectProperties(default_attributes, component.attributes);
        component.options = _mixObjectProperties(default_options, component.options);
        console.log("Vaaa :: ", component.name, C.COMPONENTS);
      }
      console.log("Vaaa", component);
      _setHeaderTitle();
      _paintProperties("attributes");
      _paintProperties("options");
      ELEMENTS.btn_delete.removeAttr("disabled");
      return evt != null;
    };
    (function() {
      Editor.Canvas.container.on("click", "[" + C.DATA_ATTRIBUTES.UID + "]", select);
      ELEMENTS.section.on("keyup", "input[type=text]", _onKeyUp);
      return ELEMENTS.btn_delete.on("click", _onDelete);
    })();
    return {
      select: select,
      unselect: unselect
    };
  })(Editor.Constants);

}).call(this);

(function() {
  Editor.Project = (function() {
    var edit, load, save, _app_json, _components, _createTree, _getJsonTree, _setBasics;
    _components = {};
    _app_json = {};
    _getJsonTree = function(tree_level) {
      var child, component, obj, _i, _len, _ref;
      obj = {};
      component = _components[tree_level.uid];
      obj.name = component.name;
      obj.type = component.type;
      obj.attributes = component.attributes;
      obj.options = component.options;
      obj.children = [];
      _ref = tree_level.children;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        obj.children.push(_getJsonTree(child));
      }
      return obj;
    };
    _createTree = function(component, container) {
      var child, el, _i, _len, _ref, _results;
      el = Editor.Canvas.appendComponent({
        component: component,
        namespace: component.type
      }, container);
      _ref = component.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        _results.push(_createTree(child, el));
      }
      return _results;
    };
    _setBasics = function() {
      var field, _i, _len, _ref, _results;
      _ref = $("[data-modal=project_modal] [name]");
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        field = _ref[_i];
        _results.push(_app_json[field.getAttribute("name")] = field.value);
      }
      return _results;
    };
    edit = function() {
      return Editor.Modal.show("project_modal");
    };
    save = function() {
      var comp, json_str, textarea, _i, _len, _ref;
      _setBasics();
      _components = Editor.Canvas.getComponents();
      _app_json.structure = [];
      _ref = Editor.Canvas.getTree();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        comp = _ref[_i];
        _app_json.structure.push(_getJsonTree(comp));
      }
      localStorage.setItem(_app_json.title, JSON.stringify(_app_json));
      console.log(_app_json);
      textarea = $("[data-modal=json_modal] > textarea");
      json_str = JSON.stringify(_app_json);
      textarea.val(json_str);
      return Editor.Modal.show("json_modal");
    };
    load = function(app_json) {
      var component, field, _i, _j, _len, _len1, _ref, _ref1, _results;
      _app_json = app_json;
      _ref = app_json.structure;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        component = _ref[_i];
        _createTree(component, Editor.Canvas.container);
      }
      Editor.Canvas.regenerateTree();
      _ref1 = $("[data-modal=project_modal] [name]");
      _results = [];
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        field = _ref1[_j];
        _results.push(field.value = _app_json[field.getAttribute("name")]);
      }
      return _results;
    };
    $(function() {
      $("[data-action=save]").on("click", save);
      return $("[data-action=edit]").on("click", edit);
    });
    return {
      save: save,
      load: load
    };
  })();

}).call(this);

(function() {
  Editor.Modal = (function() {
    var ANIMATION_END, hide, show, _current, _onHideEnd, _onShowEnd, _shadow;
    ANIMATION_END = App.Constant.EVENT.animationEnd;
    _current = null;
    _shadow = $("[data-modal-shadow]");
    _onShowEnd = function(ev) {
      _current.removeClass("showing").addClass("show");
      return _current.off(ANIMATION_END, _onShowEnd);
    };
    _onHideEnd = function(ev) {
      _current.removeClass("hidding");
      _current.off(ANIMATION_END, _onHideEnd);
      return _current = null;
    };
    show = function(id) {
      _current = $("[data-modal='" + id + "']");
      _current.on(ANIMATION_END, _onShowEnd);
      _current.addClass("showing");
      _shadow.addClass("show");
      return true;
    };
    hide = function() {
      _current.on(ANIMATION_END, _onHideEnd);
      _current.removeClass("show").addClass("hidding");
      _shadow.removeClass("show");
      return true;
    };
    $(function() {
      return $("button[data-action=close_modal]").on("click", hide);
    });
    return {
      show: show,
      hide: hide
    };
  })();

}).call(this);

(function() {
  Editor.Main = (function() {
    var init;
    init = function() {
      var saved_app;
      setTimeout(function() {
        return Editor.Components.load(Editor.Loader.getComponents());
      });
      saved_app = localStorage.getItem("App");
      if (saved_app) {
        return Editor.Project.load(JSON.parse(saved_app));
      }
    };
    $(init);
    return {
      init: init
    };
  })();

}).call(this);

World = {}
World.Event = do ->

  _listeners = {}

  listen = (eventName, callback) ->
    _listeners[eventName] = _listeners[eventName] or []
    _listeners[eventName].push callback

  inform = (eventName, data) ->
    return false unless _listeners[eventName]
    listener.call(listener, data) for listener in _listeners[eventName]

  listen: listen
  inform: inform
  listeners: -> _listeners

class World.Item

  ###*
   * [constructor description]
   * @param  {[type]} @attributes={}
   * @param  {[type]} @children=[]
   * @return {[type]}
  ###
  constructor: (@attributes={}, @children=[]) ->
    @uid = guid()
    @namespace = @constructor.name
    @listeners = {}
    child.parent = @ for child in @children

  ###*
   * [appendChild description]
   * @param  {[type]} children
   * @return {[type]}
  ###
  appendChild: (instance) ->
    instance.parent = @
    @children.push instance

  ###*
   * [bubble description]
   * @param  {[type]} event
   * @param  {[type]} data
   * @return {[type]}
  ###
  bubble: (event_name, data = {}, emmiter = @) ->
    if @parent
      result = @parent.trigger(event_name, data, emmiter)
      unless @preventBubbling is true or result is false
        @parent.bubble(event_name, data, emmiter)

  ###*
   * [tunnel description]
   * @param  {[type]} eventName
   * @param  {[type]} data
   * @return {[type]}
  ###
  tunnel: (event_name, data = {}, emmiter = @) ->
    for child in @children
      result = child.trigger(event_name, data)
      unless child.preventTunneling is true or result is false
        child.tunnel(event_name, data, emmiter)

  ###*
   * [on description]
   * @param  {[type]}   eventName
   * @param  {Function} callback
   * @return {[type]}
  ###
  on: (eventName, callback) ->
    @listeners[eventName] = @listeners[eventName] or []
    @listeners[eventName].push callback
    return @

  ###*
   * [off description]
   * @param  {[type]}   eventName [description]
   * @param  {Function} callback  [description]
   * @return {[type]}             [description]
  ###
  off: (eventName, callback) ->
    return false unless @listeners[eventName]
    index = @listeners[eventName].indexOf(callback)
    if index > -1 then @listeners[eventName].splice(index, 1)
    return @

  ###*
   * [trigger description]
   * @param  {[type]} eventName [description]
   * @param  {[type]} data      [description]
   * @return {[type]}           [description]
  ###
  trigger: (eventName, data, emmiter) ->
    if @listeners[eventName] then for callback in @listeners[eventName]
      callback.call(@, data, emmiter)
    return @

World.god = do ->

  items = {}

  class WorldItem

    ###*
     * [constructor description]
     * @param  {[type]} @attributes={}
     * @param  {[type]} @children=[]
     * @return {[type]}
    ###
    constructor: (@attributes={}, @children=[]) ->
      @uid = guid()
      @namespace = @constructor.name
      @listeners = {}
      child.parent = @ for child in @children

    ###*
     * [appendChild description]
     * @param  {[type]} children
     * @return {[type]}
    ###
    appendChild: (instance) ->
      instance.parent = @
      @children.push instance

    ###*
     * [bubble description]
     * @param  {[type]} event
     * @param  {[type]} data
     * @return {[type]}
    ###
    bubble: (event_name, data = {}, emmiter = @) ->
      if @parent
        result = @parent.trigger(event_name, data, emmiter)
        unless @preventBubbling is true or result is false
          @parent.bubble(event_name, data, emmiter)

    ###*
     * [tunnel description]
     * @param  {[type]} eventName
     * @param  {[type]} data
     * @return {[type]}
    ###
    tunnel: (event_name, data = {}, emmiter = @) ->
      for child in @children
        result = child.trigger(event_name, data)
        unless child.preventTunneling is true or result is false
          child.tunnel(event_name, data, emmiter)

    ###*
     * [listen description]
     * @param  {[type]}   eventName
     * @param  {Function} callback
     * @return {[type]}
    ###
    listen: (eventName, callback) ->
      @listeners[eventName] = @listeners[eventName] or []
      @listeners[eventName].push callback

    ###*
     * [unlisten description]
     * @param  {[type]}   eventName [description]
     * @param  {Function} callback  [description]
     * @return {[type]}             [description]
    ###
    unlisten: (eventName, callback) ->
      return false unless @listeners[eventName]
      index = @listeners[eventName].indexOf(callback)
      if index > -1 then @listeners[eventName].splice(index, 1)

    ###*
     * [trigger description]
     * @param  {[type]} eventName [description]
     * @param  {[type]} data      [description]
     * @return {[type]}           [description]
    ###
    trigger: (eventName, data, emmiter) ->
      if @listeners[eventName] then for callback in @listeners[eventName]
        callback.call(callback, data, emmiter)

  create = (attributes, children) ->
    item = new WorldItem(attributes, children)
    items[item.uid] = item
    return item
    

  create: create



guid = ->
  'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, (c) ->
    r = Math.random() * 16 | 0
    v = if c is 'x' then r else r & 3 | 8
    v.toString 16
  .toUpperCase()

Atomic = 
  DOM: $ or $$

class Atomic.Class extends World.Item

  constructor: (@attributes={}, @children=[], @options={}) ->
    super
    @attributes.id = @attributes.id or @uid
    if @constructor.template then @__createElementNode()

  ###*
   * [render description]
   * @param  {[type]} container
   * @return {[type]}
  ###
  render: (@container=@parent.el) ->
    if not @el then do @__createElementNode
    @container.append @el
    if @children.length > 0
      default_children_container = @__getDefaultContainer()
      for child in @children
        if child.attributes.container
          child.render(@__getChildContainer__(child.attributes.container))
        else child.render(default_children_container)

  # Private methods
  __createElementNode: ->
    @el = Atomic.DOM(templayed(@constructor.template)(@attributes))
    if @events and @el then for evt, callback of @events
      parts = evt.split(" ")
      el = if parts.length is 1 then @el else @el.find(parts.slice(1).join(" "))
      el.on evt, @[callback]

  __getDefaultContainer: ->
    container = @el.find("[data-children-container]")
    return if container.length then container else @el

  __getChildContainer__: (child_container) ->
    @el.find("[data-children-container=#{child_container}]")


Atomic.Loader = do ->

  _ajax = (url, callback) ->
    Atomic.DOM.ajax({
      url       : url
      dataType  : "json"
      error     : -> throw "Error loading app strcture json in #{url}"
      success   : callback
    })

  loadJson = (url, callback) ->
    app = {}
    _ajax(url, (appJson) ->
      app[attr] = value for attr, value of appJson when attr isnt "structure"
      app.structure = loadStructure(appJson.structure)
      callback.call(callback, app)
    )

  loadStructure = (items, parent, callback) ->
    structure = []
    structure.push(createStructure(item, parent)) for item in items
    structure

  createStructure = (item, parent) ->
    instance = new App[item.type][item.name](item.attributes, [], item.options)
    if parent then parent.appendChild(instance)
    if item.children then for child in item.children
      createStructure(child, instance) 
    return instance


  loadJson        : loadJson
  createStructure : createStructure

App = do ->

  init = (json_url, callback) ->
    Atomic.Loader.loadJson json_url, (app) ->
      App.Cache.init(app.structure)
      app_container = Atomic.DOM(document.body)
      component.render(app_container) for component in app.structure
      callback?.call(app)
      if app.startRoute
        App.Router.init(app.startRoute, app.startAnimation)


  init      : init
  Base      : Atomic.Class
  Atom      : {}
  Molecule  : {}
  Organism  : {}
  Template  : {}

App.Constant = do ->

  _width = window.innerWidth
  _height = window.innerHeight

  cssTransform = 
    'WebkitTransform'   : '-webkit-transform'
    'MozTransform'      : '-moz-transform'
    'OTransform'        : '-o-transform'
    'msTransform'       : '-ms-transform'
    'transform'         : 'transform'

  transitionFinishedEvents =
    'WebkitTransition'  : 'webkitTransitionEnd'
    'MozTransition'     : 'transitionend'
    'OTransition'       : 'oTransitionEnd'
    'msTransition'      : 'msTransitionEnd'
    'transition'        : 'transitionEnd'

  animationFinishedEvents =
    'WebkitAnimation'   : 'webkitAnimationEnd'
    'MozAnimation'      : 'animationend'
    'OAnimation'        : 'oAnimationEnd'
    'msAnimation'       : 'msAnimationEnd'
    'animation'         : 'animationEnd'

  isTouchScreen = Modernizr.touch


  TOUCH: isTouchScreen

  DEVICE:
    orientation : if _width > _height then 'portait' else 'landscape'
    width       : window.innerWidth
    height      : window.innerHeight

  EVENT:
    animationEnd  : animationFinishedEvents[Modernizr.prefixed("Animation")]
    transitionEnd : transitionFinishedEvents[Modernizr.prefixed("Transition")]
    touchStart    : if isTouchScreen then "touchstart" else "mousedown"
    touchMove     : if isTouchScreen then "touchmove" else "mousemove"
    touchEnd      : if isTouchScreen then "touchend" else "mouseup"
    
  CSS:
    transform     : cssTransform[Modernizr.prefixed("transform")]


App.Gesture = do ->

  STARTED = false

  _events = {}
  _callbacks = []
  _lastEvent = null
  _gesture = {}

  _touchstart = (event) ->
    _initialize(event)
    _handleEvent(event, "start")

  _touchmove = (event) ->
    return unless _gesture.startPositions
    _gesture.positions = _getFingersPositions(event)
    _gesture.deltaTime = new Date() - _gesture.startTime
    _setDeltaPositions()
    _handleEvent(event, "move")

  _touchend = (event) ->
    return unless _gesture.startPositions
    _gesture.deltaTime = new Date() - _gesture.startTime
    _handleEvent(event, "end")
    _initialize()

  _touchcancel = (event) ->
    _handleEvent(event, "cancel")
    _initialize()

  _initialize = (evt) ->
    _lastEvent = null
    _gesture = {}
    return unless evt?
    _gesture.startTime = new Date()
    _gesture.startPositions = _getFingersPositions(evt)

  _getFingersPositions = (evt) ->
    positions = []
    positions.push({x: touch.pageX, y: touch.pageY}) for touch in _getTouches(evt)
    positions

  _getTouches = (evt) ->
    if evt.touches? then evt.touches else [{pageX: evt.pageX, pageY: evt.pageY}]

  _setDeltaPositions = ->
    i = 0
    _gesture.deltas = []
    for position in _gesture.positions
      _gesture.deltas.push
        x: (position.x - _gesture.startPositions[i].x)
        y: (position.y - _gesture.startPositions[i++].y)
    _gesture.deltaTime = (new Date()) - _gesture.startTime

  _handleEvent = (event, moment) ->
    _lastEvent = event
    _gestureClone = JSON.parse(JSON.stringify(_gesture))
    for cb in _callbacks when !!cb[moment]
      cb[moment].call(null, event, _gestureClone)


  document.body.addEventListener(App.Constant.EVENT.touchStart, _touchstart)
  document.body.addEventListener(App.Constant.EVENT.touchMove, _touchmove)
  document.body.addEventListener(App.Constant.EVENT.touchEnd, _touchend)
  document.body.addEventListener("touchcancel", _touchcancel)


  register = (events, callbacks) ->
    _callbacks.push(callbacks)
    for evt in events
      _events[evt] = new CustomEvent(evt, {bubbles: true, cancelable: true})

  trigger = (event_name, data) ->
    evt = _events[event_name]
    evt.originalEvent = _lastEvent
    evt.gestureData = JSON.parse(JSON.stringify(_gesture))
    evt.gestureData[key] = data[key] for key of data when data
    _lastEvent.target.dispatchEvent(evt)

  asyncTrigger = (event_name, target, data) ->
    target.dispatchEvent(_events[event_name], data)


  register      : register
  trigger       : trigger
  asyncTrigger  : asyncTrigger



App.Gesture.register ["tap", "hold", "doubleTap", "singleTap"], do ->

  HOLD_TIME   = 300
  TAP_TIME    = 300
  DOUBLE_TIME = 450
  GAP         = 3

  _possibleTap    = false
  _possibleDouble = false
  _holdTimeout    = null
  _simpleTimeout  = null

  start = (event, gd) ->
    _possibleTap = gd.startPositions.length is 1
    _holdTimeout = setTimeout(_lazyHold(event.target), HOLD_TIME)  if _possibleTap
    return

  move = (event, gd) ->
    delta = gd.deltas[0]
    _possibleTap = gd.deltas.length is 1 and Math.abs(delta.x) < GAP and Math.abs(delta.y) < GAP
    unless _possibleTap
      clearTimeout(_holdTimeout)
      clearTimeout(_simpleTimeout)
    return

  end = (event, gd) ->
    if _possibleTap and gd.deltaTime < TAP_TIME
      clearTimeout(_holdTimeout)
      App.Gesture.trigger("tap")
      if _possibleDouble
        clearTimeout(_simpleTimeout)
        App.Gesture.trigger("doubleTap")
        _possibleDouble = false
      else
        _simpleTimeout = setTimeout(_lazySimpleTap(event.target), DOUBLE_TIME)
        _possibleDouble = true
    return

  _lazyHold = (target) -> ->
    App.Gesture.asyncTrigger "hold", target

  _lazySimpleTap = (target) -> ->
    App.Gesture.asyncTrigger "singleTap", target
    _possibleDouble = false


  start: start
  move: move
  end: end
 

App.Gesture.register ["swiping", "swipingHorizontal", "swipingVertical", "swipe"], do ->

    _first    = true
    _swiping  = false
    _swipingH = false
    _swipingV = false

    _log = (txt, clear) ->
      if clear then $("#page1 header h1").text(txt)
      else $("#page1 header h1").text($("#page1 header h1").text() + "; "+ txt)

    start: ->
      _first    = true
      _swiping  = false
      _swipingH = false
      _swipingV = false
      return true

    move: (event, gd) ->
      if gd.deltas.length is 1
        if not _swiping
          _swipingH = Math.abs(gd.deltas[0].x / gd.deltas[0].y) >= 2
          _swipingV = Math.abs(gd.deltas[0].y / gd.deltas[0].x) >= 2

        App.Gesture.trigger("swiping", first: _first)
        if _swipingH then App.Gesture.trigger("swipingHorizontal", first: _first)
        if _swipingV then App.Gesture.trigger("swipingVertical", first: _first)
        _swiping = true
        _first = false
      return true

    end: (event, gd) ->
      if _swiping then App.Gesture.trigger("swipe")
      return true

Commons = Commons or {}

Commons.data = ->	
  if @attributes.data
  	for key, value of @attributes.data
  	  @el.attr "data-#{key}", value


Commons = Commons or {}

Commons.router = -> 
  if @options?.router
    parts = @options.router.split(":")
    @el.on "tap", (ev) =>
      App.Router[parts[0]](parts[1])
      ev.originalEvent.preventDefault()
      ev.originalEvent.stopPropagation()

class App.Atom.Button extends Atomic.Class

  @type: "Button"
  @template: """
    <button class="{{class}}">
        <span class="icon {{icon}}"></span>
        {{text}}
    </button>
  """

  constructor: ->
    super
    Commons.router.call(@)
    Commons.data.call(@)

class App.Atom.Input extends Atomic.Class

  @type: "Input"
  @template: """
    <input type="{{type}}" placeholder="{{placeholder}}" class="{{class}}" name="{{name}}" />
  """

  value: -> @el.val()

class App.Atom.Label extends Atomic.Class

  @type: "Label"
  @template: """
    <label class="{{class}}">{{text}}</label>
  """

class App.Atom.Li extends Atomic.Class

  @type: "Li"
  @template: """
    <li>
    	{{text}}
    	<small>{{description}}</small>
    </li>
  """

  constructor: ->
    super
    Commons.router.call(@)


class App.Atom.Loading extends Atomic.Class

  @type: "Loading"
  @template: """
  	<div class="atom_loading">
  		<div class="">{{text}}</div>
  		<span class="icon looping"></span>
  	</div>
  """
class App.Atom.P extends Atomic.Class

  @type: "Label"
  @template: """
    <p class="{{class}}">{{text}}</p>
  """

class App.Atom.Select extends Atomic.Class

  @type: "Select"
  @template: """
    <select name="{{name}}" class="{{class}}" id="{{id}}">
        {{#options}}
            <option value="{{value}}">{{label}}</option>
        {{/options}}
    </select>
  """

  constructor: ->
    super
    if @attributes.value then @el.val(attributes.value)

  value: -> @el.val()

class App.Atom.Textarea extends Atomic.Class

  @type: "Textarea"
  @template: """
    <textarea class="{{class}}"></textarea>
  """

  value: -> @el.val()

class App.Atom.Title extends Atomic.Class

  @type: "Title"
  @template: """
    <h{{size}} class="{{class}}">{{text}}</h{{size}}>
  """

  constructor: ->
    super

class App.Molecule.Footer extends Atomic.Class

  @type: "Footer"
  @template: """
    <footer>
        <nav data-children-container="navigation"></nav>
    </footer>
  """

  render: ->
    super
    @el[0].addEventListener "touchmove", (e) -> e.preventDefault()

class App.Molecule.Form extends Atomic.Class

  @type     : "Form"
  @template : """
    <form class="{{class}}" id="{{id}}" onsubmit="return false;">
      <h1>{{title}}</h1>
    </form>
  """

  constructor: ->
    super
    submit_attributes = {text: "Submit", class: "big fluid accept"}
    @submit = new App.Atom.Button(submit_attributes)
    @appendChild(@submit)
    @submit.el.bind("tap", (=> console.log @value()))

  onSubmit: (callback) ->
    @submit.el.bind("tap", (=> callback.call @, @value()))

  value: ->
    values = {}
    for child in @children when child.value?
      values[child.attributes.name] = child.value()
    return values

class App.Molecule.Header extends Atomic.Class

  @type: "Header"
  @template: """
    <header>
        <h1>{{title}}</h1>
        <nav class="left" data-children-container="left"></nav>
        <nav class="right" data-children-container="right"></nav>
    </header>
  """

  render: ->
    super
    @el[0].addEventListener "touchmove", (e) -> e.preventDefault()
    buttons = @children.filter (item) -> item.namespace is "Button"
    for button in buttons
      if button.visibleOnSection
        button.el.attr "data-show-section", button.visibleOnSection
      if button.hiddenOnSection
        button.el.attr "data-hide-section", button.hiddenOnSection


class App.Molecule.List extends Atomic.Class

  @type: "List"
  @template: """
    <ul class="{{class}}"></ul>
  """
class App.Molecule.Nav extends Atomic.Class

  @type: "Nav"
  @template: """
    <nav class="{{class}}"></nav>
  """
class App.Molecule.Search extends Atomic.Class

	@template: """

	"""

	constructor: (@attributes={}, @children=[], @options={}) ->
		super
		console.log "Vaaa", @

class App.Organism.Aside extends Atomic.Class

  @type: "Aside"
  @template: """
    <aside class="{{class}}" id="{{id}}"></aside>
  """

  render: ->
    super
    @sections = {}
    for child in @children
      if child.constructor.name is "Section"
        @sections[child.attributes.id] = child

  activate: ->
    @el.addClass("active")

  deactivate: ->
    @el.removeClass("active")
class App.Organism.Modal extends Atomic.Class

  @type: "Modal"
  @template: """
    <div id="{{id}}" data-modal="{{animation}}" class="{{class}}"></div>
  """

class App.Organism.Section extends Atomic.Class

  @PULL_GAP: 70
  @type: "Section"
  @template: """
    <section id="{{id}}" class="{{class}}"></section>
  """

  @_onTransitionEnd = (pullLayer) -> 
    (ev) =>
      section = $(ev.currentTarget)
      if section.attr("data-state") is "pull-out"
        pullLayer.removeClass("visible")

  swipped : 0
  pulled  : false

  _createPullLayer: (parent) ->
    @pullLayer = $ """
      <div class=\"pullrefresh\">
        <p class="font big centered margins">Pull & Refresh</p>
        <p class="font centered"><span class="icon looping"></span></p>
      </div>
    """
    parent.append(@pullLayer)

  _pulling: (ev) =>
    delta = ev.gestureData.deltas[0].y
    if !@pulled
      if ev.currentTarget.scrollTop is 0
        diff = Math.max(delta, 0)
        if diff > 0
          @pullLayer.addClass("visible")
          @el.removeAttr("data-state")
          @swipped = diff * 0.35
          @el.css(App.Constant.CSS.transform, "translateY(#{@swipped}px)")
          ev.originalEvent.preventDefault()
    else
      ev.originalEvent.preventDefault()

    return true



  pull: (show=true) ->
    @el.removeAttr("style")
    @pulled = show
    if show
      @el.attr("data-state", "pull-in")
      @trigger("pull")
    else
      @el.attr("data-state", "pull-out")
      @trigger("unpull")

  render: ->
    super
    if @options.pullable
      @_createPullLayer(@parent.el)
      onEnd = @constructor._onTransitionEnd(@pullLayer)
      @el.on(App.Constant.EVENT.transitionEnd, onEnd)
      @el.on "swipingVertical", @_pulling
      @el.on "swipe", (ev) =>
        if @swipped > 0
          @pull(@swipped > @constructor.PULL_GAP)
          @swipped = 0
        return true

class App.Template.Article extends Atomic.Class

  @type         : "Article"
  @template     : """
    <article id="{{id}}" class="{{class}}"></article>
  """

  # Router animation functions
  @_animations_in_progress = 0

  @_startRouterAnimation = (element, animation, direction) =>
    @_animations_in_progress += 1
    element.addClass("active").attr("data-animation", animation)
    element.attr("data-direction", direction)

  @_onAnimationEnd = (ev) =>
    el = Atomic.DOM(ev.currentTarget)
    article = App.Cache.article(el.attr("id"))
    if el.attr("data-direction") in ["out", "back-out"]
      el.removeClass("active")
      article.trigger("unload")
    else
      article.trigger("load")
      if article.aside
        App.Cache.aside(article.aside).el.addClass("active")

    el.removeAttr("data-direction").removeAttr("data-animation").removeAttr("data-start-animation")
    setTimeout (=> @_animations_in_progress--), 50

  # Aside transition functions
  @ASIDE_WIDTH = 250
  @_swiped_px = 0
  @_aside_open = false
  @_aside_transforming = false
  @_aside_callback = null

  @_onSwipingHorizontal = (ev) =>
    if @_aside_open then ev.preventDefault()
    else
      delta = Math.max(ev.gestureData.deltas[0].x, 0)
      if delta > @ASIDE_WIDTH
        delta = ((delta - @ASIDE_WIDTH) * 0.2) + @ASIDE_WIDTH

      @_swiped_px = delta
      Atomic.DOM(ev.currentTarget)
        .css(App.Constant.CSS.transform, "translateX(#{delta}px)")
      ev.originalEvent.preventDefault()

  @_onSwipe = (ev) =>
    article = Atomic.DOM(ev.currentTarget)
    if (!@_aside_open and @_swiped_px > 100) or @_aside_open
      App.Cache.article().toggleAside()
    else if @_swiped_px > 0
      article.on(App.Constant.EVENT.transitionEnd, @_onTransitionEnd)
      article.addClass("aside")
    article.removeAttr("style")
    @_swiped_px = 0

  @_onTransitionEnd = (ev) =>
    target = Atomic.DOM(ev.currentTarget)
    target.off(App.Constant.EVENT.transitionEnd, @_onTransitionEnd)
    if target.hasClass("aside") and !target.attr("data-aside")
      target.removeClass("aside")
      @_aside_open = false
    else @_aside_open = true
    @_aside_transforming = false
    if @_aside_callback
      @_aside_callback.call(@)
      @_aside_callback = null

  @_onTap = =>
    if @_aside_open and !@_aside_transforming
      App.Cache.article().toggleAside()


  # Instance methods
  constructor: (attributes={}, children=[], options={}) ->
    super
    @el.on App.Constant.EVENT.animationEnd, @constructor._onAnimationEnd
    @animation = options.animation
    @aside = options.aside
    if @aside
      @el.on("touchend", @constructor._onTap)
      @el.on("swipingHorizontal", @constructor._onSwipingHorizontal)
      @el.on("swipe", @constructor._onSwipe)

  render: ->
    super
    @sections = {}
    for child in @children when child.constructor.type is "Section"
      @sections[child.attributes.id] = child

  show: (is_back=false, start_animation="fade") ->
    current = App.Cache.article()
    @trigger("beforeload")
    App.Cache.setArticle(@)
    App.Cache.aside(@aside).deactivate() if @aside
    App.Cache.aside(current.aside).deactivate() if current?.aside
    @el.addClass("active")
    unless current
      @el.attr("data-start-animation", "start_#{start_animation}")
      return

    current.trigger("beforeunload")
    thisArgs    = if is_back then [@el, current.animation, "back-in"] else [@el, @animation, "in"]
    remoteArgs  = if is_back then [current.el, current.animation, "back-out"] else [current.el, @animation, "out"]
    @constructor._startRouterAnimation.apply(null, thisArgs)
    @constructor._startRouterAnimation.apply(null, remoteArgs)

  section: (id) ->
    sections = @el.children("section")
    sections.removeClass("active").filter("##{id}").addClass("active")
    @trigger("load:#{id}")
    if @constructor._aside_open then @toggleAside()

  toggleAside: (callback) ->
    return false if @constructor._aside_transforming is true
    @constructor._aside_callback = callback
    @constructor._aside_transforming = true
    App.Cache.aside(@aside).el.addClass("active")
    @el.on App.Constant.EVENT.transitionEnd, @constructor._onTransitionEnd
    if @el.attr("data-aside")
      @constructor._aside_open = false
      @el.addClass("aside").removeAttr("data-aside")
    else
      @constructor._aside_open = true
      @el.addClass("aside").attr("data-aside", "show")




App.Cache = do ->

  _cache = {}

  init = (app) ->
    _cacheCommons app

  setArticle = (instance) -> _cache.currentArticle = instance

  _cacheCommons = (app) ->
    for component in app when component.constructor.type in ["Article", "Aside"]
      node_name = component.constructor.type.toLowerCase()
      _cache[node_name] = _cache[node_name] or {}
      _cache[node_name][component.attributes.id] = component


  init        : init
  setArticle  : setArticle
  aside       : (id) -> _cache.aside[id]
  article     : (id) -> if id then _cache.article[id] else _cache.currentArticle

  register    : (namespace, item) ->
    _cache[namespace] = _cache[namespace] or {}
    _cache[namespace][item.attributes.id] = item

App.Modal = do ->

  _current    = null
  _blockDiv   = document.createElement("div")
  _blockDiv.setAttribute("data-block-modal", "default")
  document.body.appendChild(_blockDiv)

  _onAnimationEnd = (ev) ->
    target = Atomic.DOM(ev.currentTarget)
    if target.attr("data-direction") is "in" 
      target.attr("data-status", "active")
    else
      target.removeAttr("data-status")
    target.removeAttr("data-direction")
    target.off(App.Constant.EVENT.animationEnd, _onAnimationEnd)

  show = (id) ->
    _current = Atomic.DOM(document.getElementById(id))
    if _current.length is 1
      _blockDiv.className = "active"
      _current.on(App.Constant.EVENT.animationEnd, _onAnimationEnd)
      _current.attr("data-direction", "in")

  hide = ->
    if _current
      _blockDiv.removeAttribute("class")
      _current.on(App.Constant.EVENT.animationEnd, _onAnimationEnd)
      _current.attr("data-direction", "out")

    _current = null


  show: show
  hide: hide

App.Router = do ->

  _routes = []

  _showArticle = (id, is_back, start_animation) -> ->
    instance = App.Cache.article(id)
    console.log instance
    instance.show(is_back, start_animation)
    if is_back then _routes.length--
    else _routes.push(id)

  init = (article_id, start_animation) ->
    article(article_id, false, start_animation)

  article = (id, is_back=false, start_animation) ->
    unless App.Template.Article._animations_in_progress > 0
      App.Modal.hide()
      funct = _showArticle(id, is_back, start_animation)
      if App.Template.Article._aside_open
        App.Cache.article().toggleAside(funct)
      else funct()

  section = (id) ->
    App.Modal.hide()
    App.Cache.article().section(id)

  modal = (id) ->
    if id is "hide" then App.Modal.hide()
    else App.Modal.show(id)

  aside = ->
    App.Cache.article().toggleAside()

  back = ->
    len = _routes.length
    if len <= 1 then return false
    article _routes[len - 2], true


  init        : init
  article     : article
  section     : section
  modal       : modal
  back        : back
  aside       : aside

@App = App
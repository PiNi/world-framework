(function() {
  var World, guid;

  World = {};

  World.Event = (function() {
    var inform, listen, _listeners;
    _listeners = {};
    listen = function(eventName, callback) {
      _listeners[eventName] = _listeners[eventName] || [];
      return _listeners[eventName].push(callback);
    };
    inform = function(eventName, data) {
      var listener, _i, _len, _ref, _results;
      if (!_listeners[eventName]) {
        return false;
      }
      _ref = _listeners[eventName];
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        listener = _ref[_i];
        _results.push(listener.call(listener, data));
      }
      return _results;
    };
    return {
      listen: listen,
      inform: inform,
      listeners: function() {
        return _listeners;
      }
    };
  })();

  World.Item = (function() {

    /**
     * [constructor description]
     * @param  {[type]} @attributes={}
     * @param  {[type]} @children=[]
     * @return {[type]}
     */
    function Item(attributes, children) {
      var child, _i, _len, _ref;
      this.attributes = attributes != null ? attributes : {};
      this.children = children != null ? children : [];
      this.uid = guid();
      this.namespace = this.constructor.name;
      this.listeners = {};
      _ref = this.children;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        child.parent = this;
      }
    }


    /**
     * [appendChild description]
     * @param  {[type]} children
     * @return {[type]}
     */

    Item.prototype.appendChild = function(instance) {
      instance.parent = this;
      return this.children.push(instance);
    };


    /**
     * [bubble description]
     * @param  {[type]} event
     * @param  {[type]} data
     * @return {[type]}
     */

    Item.prototype.bubble = function(event_name, data, emmiter) {
      var result;
      if (data == null) {
        data = {};
      }
      if (emmiter == null) {
        emmiter = this;
      }
      if (this.parent) {
        result = this.parent.trigger(event_name, data, emmiter);
        if (!(this.preventBubbling === true || result === false)) {
          return this.parent.bubble(event_name, data, emmiter);
        }
      }
    };


    /**
     * [tunnel description]
     * @param  {[type]} eventName
     * @param  {[type]} data
     * @return {[type]}
     */

    Item.prototype.tunnel = function(event_name, data, emmiter) {
      var child, result, _i, _len, _ref, _results;
      if (data == null) {
        data = {};
      }
      if (emmiter == null) {
        emmiter = this;
      }
      _ref = this.children;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        child = _ref[_i];
        result = child.trigger(event_name, data);
        if (!(child.preventTunneling === true || result === false)) {
          _results.push(child.tunnel(event_name, data, emmiter));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };


    /**
     * [on description]
     * @param  {[type]}   eventName
     * @param  {Function} callback
     * @return {[type]}
     */

    Item.prototype.on = function(eventName, callback) {
      this.listeners[eventName] = this.listeners[eventName] || [];
      this.listeners[eventName].push(callback);
      return this;
    };


    /**
     * [off description]
     * @param  {[type]}   eventName [description]
     * @param  {Function} callback  [description]
     * @return {[type]}             [description]
     */

    Item.prototype.off = function(eventName, callback) {
      var index;
      if (!this.listeners[eventName]) {
        return false;
      }
      index = this.listeners[eventName].indexOf(callback);
      if (index > -1) {
        this.listeners[eventName].splice(index, 1);
      }
      return this;
    };


    /**
     * [trigger description]
     * @param  {[type]} eventName [description]
     * @param  {[type]} data      [description]
     * @return {[type]}           [description]
     */

    Item.prototype.trigger = function(eventName, data, emmiter) {
      var callback, _i, _len, _ref;
      if (this.listeners[eventName]) {
        _ref = this.listeners[eventName];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          callback = _ref[_i];
          callback.call(this, data, emmiter);
        }
      }
      return this;
    };

    return Item;

  })();

  World.god = (function() {
    var WorldItem, create, items;
    items = {};
    WorldItem = (function() {

      /**
       * [constructor description]
       * @param  {[type]} @attributes={}
       * @param  {[type]} @children=[]
       * @return {[type]}
       */
      function WorldItem(attributes, children) {
        var child, _i, _len, _ref;
        this.attributes = attributes != null ? attributes : {};
        this.children = children != null ? children : [];
        this.uid = guid();
        this.namespace = this.constructor.name;
        this.listeners = {};
        _ref = this.children;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          child.parent = this;
        }
      }


      /**
       * [appendChild description]
       * @param  {[type]} children
       * @return {[type]}
       */

      WorldItem.prototype.appendChild = function(instance) {
        instance.parent = this;
        return this.children.push(instance);
      };


      /**
       * [bubble description]
       * @param  {[type]} event
       * @param  {[type]} data
       * @return {[type]}
       */

      WorldItem.prototype.bubble = function(event_name, data, emmiter) {
        var result;
        if (data == null) {
          data = {};
        }
        if (emmiter == null) {
          emmiter = this;
        }
        if (this.parent) {
          result = this.parent.trigger(event_name, data, emmiter);
          if (!(this.preventBubbling === true || result === false)) {
            return this.parent.bubble(event_name, data, emmiter);
          }
        }
      };


      /**
       * [tunnel description]
       * @param  {[type]} eventName
       * @param  {[type]} data
       * @return {[type]}
       */

      WorldItem.prototype.tunnel = function(event_name, data, emmiter) {
        var child, result, _i, _len, _ref, _results;
        if (data == null) {
          data = {};
        }
        if (emmiter == null) {
          emmiter = this;
        }
        _ref = this.children;
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];
          result = child.trigger(event_name, data);
          if (!(child.preventTunneling === true || result === false)) {
            _results.push(child.tunnel(event_name, data, emmiter));
          } else {
            _results.push(void 0);
          }
        }
        return _results;
      };


      /**
       * [listen description]
       * @param  {[type]}   eventName
       * @param  {Function} callback
       * @return {[type]}
       */

      WorldItem.prototype.listen = function(eventName, callback) {
        this.listeners[eventName] = this.listeners[eventName] || [];
        return this.listeners[eventName].push(callback);
      };


      /**
       * [unlisten description]
       * @param  {[type]}   eventName [description]
       * @param  {Function} callback  [description]
       * @return {[type]}             [description]
       */

      WorldItem.prototype.unlisten = function(eventName, callback) {
        var index;
        if (!this.listeners[eventName]) {
          return false;
        }
        index = this.listeners[eventName].indexOf(callback);
        if (index > -1) {
          return this.listeners[eventName].splice(index, 1);
        }
      };


      /**
       * [trigger description]
       * @param  {[type]} eventName [description]
       * @param  {[type]} data      [description]
       * @return {[type]}           [description]
       */

      WorldItem.prototype.trigger = function(eventName, data, emmiter) {
        var callback, _i, _len, _ref, _results;
        if (this.listeners[eventName]) {
          _ref = this.listeners[eventName];
          _results = [];
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            callback = _ref[_i];
            _results.push(callback.call(callback, data, emmiter));
          }
          return _results;
        }
      };

      return WorldItem;

    })();
    create = function(attributes, children) {
      var item;
      item = new WorldItem(attributes, children);
      items[item.uid] = item;
      return item;
    };
    return {
      create: create
    };
  })();

  guid = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r, v;
      r = Math.random() * 16 | 0;
      v = c === 'x' ? r : r & 3 | 8;
      return v.toString(16);
    }).toUpperCase();
  };

  this.World = World;

}).call(this);

Atomic.Loader = do ->

  _ajax = (url, callback) ->
    Atomic.DOM.ajax({
      url       : url
      dataType  : "json"
      error     : -> throw "Error loading app strcture json in #{url}"
      success   : callback
    })

  loadJson = (url, callback) ->
    app = {}
    _ajax(url, (appJson) ->
      app[attr] = value for attr, value of appJson when attr isnt "structure"
      app.structure = loadStructure(appJson.structure)
      callback.call(callback, app)
    )

  loadStructure = (items, parent, callback) ->
    structure = []
    structure.push(createStructure(item, parent)) for item in items
    structure

  createStructure = (item, parent) ->
    instance = new App[item.type][item.name](item.attributes, [], item.options)
    if parent then parent.appendChild(instance)
    if item.children then for child in item.children
      createStructure(child, instance) 
    return instance


  loadJson        : loadJson
  createStructure : createStructure

App.Modal = do ->

  _current    = null
  _blockDiv   = document.createElement("div")
  _blockDiv.setAttribute("data-block-modal", "default")
  document.body.appendChild(_blockDiv)

  _onAnimationEnd = (ev) ->
    target = Atomic.DOM(ev.currentTarget)
    if target.attr("data-direction") is "in" 
      target.attr("data-status", "active")
    else
      target.removeAttr("data-status")
    target.removeAttr("data-direction")
    target.off(App.Constant.EVENT.animationEnd, _onAnimationEnd)

  show = (id) ->
    _current = Atomic.DOM(document.getElementById(id))
    if _current.length is 1
      _blockDiv.className = "active"
      _current.on(App.Constant.EVENT.animationEnd, _onAnimationEnd)
      _current.attr("data-direction", "in")

  hide = ->
    if _current
      _blockDiv.removeAttribute("class")
      _current.on(App.Constant.EVENT.animationEnd, _onAnimationEnd)
      _current.attr("data-direction", "out")

    _current = null


  show: show
  hide: hide

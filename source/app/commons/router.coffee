Commons = Commons or {}

Commons.router = -> 
  if @options?.router
    parts = @options.router.split(":")
    @el.on "tap", (ev) =>
      App.Router[parts[0]](parts[1])
      ev.originalEvent.preventDefault()
      ev.originalEvent.stopPropagation()

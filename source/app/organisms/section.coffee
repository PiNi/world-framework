class App.Organism.Section extends Atomic.Class

  @PULL_GAP: 70
  @type: "Section"
  @template: """
    <section id="{{id}}" class="{{class}}"></section>
  """

  @_onTransitionEnd = (pullLayer) -> 
    (ev) =>
      section = $(ev.currentTarget)
      if section.attr("data-state") is "pull-out"
        pullLayer.removeClass("visible")

  swipped : 0
  pulled  : false

  _createPullLayer: (parent) ->
    @pullLayer = $ """
      <div class=\"pullrefresh\">
        <p class="font big centered margins">Pull & Refresh</p>
        <p class="font centered"><span class="icon looping"></span></p>
      </div>
    """
    parent.append(@pullLayer)

  _pulling: (ev) =>
    delta = ev.gestureData.deltas[0].y
    if !@pulled
      if ev.currentTarget.scrollTop is 0
        diff = Math.max(delta, 0)
        if diff > 0
          @pullLayer.addClass("visible")
          @el.removeAttr("data-state")
          @swipped = diff * 0.35
          @el.css(App.Constant.CSS.transform, "translateY(#{@swipped}px)")
          ev.originalEvent.preventDefault()
    else
      ev.originalEvent.preventDefault()

    return true



  pull: (show=true) ->
    @el.removeAttr("style")
    @pulled = show
    if show
      @el.attr("data-state", "pull-in")
      @trigger("pull")
    else
      @el.attr("data-state", "pull-out")
      @trigger("unpull")

  render: ->
    super
    if @options.pullable
      @_createPullLayer(@parent.el)
      onEnd = @constructor._onTransitionEnd(@pullLayer)
      @el.on(App.Constant.EVENT.transitionEnd, onEnd)
      @el.on "swipingVertical", @_pulling
      @el.on "swipe", (ev) =>
        if @swipped > 0
          @pull(@swipped > @constructor.PULL_GAP)
          @swipped = 0
        return true

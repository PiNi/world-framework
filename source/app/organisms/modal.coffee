class App.Organism.Modal extends Atomic.Class

  @type: "Modal"
  @template: """
    <div id="{{id}}" data-modal="{{animation}}" class="{{class}}"></div>
  """

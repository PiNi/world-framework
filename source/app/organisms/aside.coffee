class App.Organism.Aside extends Atomic.Class

  @type: "Aside"
  @template: """
    <aside class="{{class}}" id="{{id}}"></aside>
  """

  render: ->
    super
    @sections = {}
    for child in @children
      if child.constructor.name is "Section"
        @sections[child.attributes.id] = child

  activate: ->
    @el.addClass("active")

  deactivate: ->
    @el.removeClass("active")
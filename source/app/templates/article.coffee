class App.Template.Article extends Atomic.Class

  @type         : "Article"
  @template     : """
    <article id="{{id}}" class="{{class}}"></article>
  """

  # Router animation functions
  @_animations_in_progress = 0

  @_startRouterAnimation = (element, animation, direction) =>
    @_animations_in_progress += 1
    element.addClass("active").attr("data-animation", animation)
    element.attr("data-direction", direction)

  @_onAnimationEnd = (ev) =>
    el = Atomic.DOM(ev.currentTarget)
    article = App.Cache.article(el.attr("id"))
    if el.attr("data-direction") in ["out", "back-out"]
      el.removeClass("active")
      article.trigger("unload")
    else
      article.trigger("load")
      if article.aside
        App.Cache.aside(article.aside).el.addClass("active")

    el.removeAttr("data-direction").removeAttr("data-animation").removeAttr("data-start-animation")
    setTimeout (=> @_animations_in_progress--), 50

  # Aside transition functions
  @ASIDE_WIDTH = 250
  @_swiped_px = 0
  @_aside_open = false
  @_aside_transforming = false
  @_aside_callback = null

  @_onSwipingHorizontal = (ev) =>
    if @_aside_open then ev.preventDefault()
    else
      delta = Math.max(ev.gestureData.deltas[0].x, 0)
      if delta > @ASIDE_WIDTH
        delta = ((delta - @ASIDE_WIDTH) * 0.2) + @ASIDE_WIDTH

      @_swiped_px = delta
      Atomic.DOM(ev.currentTarget)
        .css(App.Constant.CSS.transform, "translateX(#{delta}px)")
      ev.originalEvent.preventDefault()

  @_onSwipe = (ev) =>
    article = Atomic.DOM(ev.currentTarget)
    if (!@_aside_open and @_swiped_px > 100) or @_aside_open
      App.Cache.article().toggleAside()
    else if @_swiped_px > 0
      article.on(App.Constant.EVENT.transitionEnd, @_onTransitionEnd)
      article.addClass("aside")
    article.removeAttr("style")
    @_swiped_px = 0

  @_onTransitionEnd = (ev) =>
    target = Atomic.DOM(ev.currentTarget)
    target.off(App.Constant.EVENT.transitionEnd, @_onTransitionEnd)
    if target.hasClass("aside") and !target.attr("data-aside")
      target.removeClass("aside")
      @_aside_open = false
    else @_aside_open = true
    @_aside_transforming = false
    if @_aside_callback
      @_aside_callback.call(@)
      @_aside_callback = null

  @_onTap = =>
    if @_aside_open and !@_aside_transforming
      App.Cache.article().toggleAside()


  # Instance methods
  constructor: (attributes={}, children=[], options={}) ->
    super
    @el.on App.Constant.EVENT.animationEnd, @constructor._onAnimationEnd
    @animation = options.animation
    @aside = options.aside
    if @aside
      @el.on("touchend", @constructor._onTap)
      @el.on("swipingHorizontal", @constructor._onSwipingHorizontal)
      @el.on("swipe", @constructor._onSwipe)

  render: ->
    super
    @sections = {}
    for child in @children when child.constructor.type is "Section"
      @sections[child.attributes.id] = child

  show: (is_back=false, start_animation="fade") ->
    current = App.Cache.article()
    @trigger("beforeload")
    App.Cache.setArticle(@)
    App.Cache.aside(@aside).deactivate() if @aside
    App.Cache.aside(current.aside).deactivate() if current?.aside
    @el.addClass("active")
    unless current
      @el.attr("data-start-animation", "start_#{start_animation}")
      return

    current.trigger("beforeunload")
    thisArgs    = if is_back then [@el, current.animation, "back-in"] else [@el, @animation, "in"]
    remoteArgs  = if is_back then [current.el, current.animation, "back-out"] else [current.el, @animation, "out"]
    @constructor._startRouterAnimation.apply(null, thisArgs)
    @constructor._startRouterAnimation.apply(null, remoteArgs)

  section: (id) ->
    sections = @el.children("section")
    sections.removeClass("active").filter("##{id}").addClass("active")
    @trigger("load:#{id}")
    if @constructor._aside_open then @toggleAside()

  toggleAside: (callback) ->
    return false if @constructor._aside_transforming is true
    @constructor._aside_callback = callback
    @constructor._aside_transforming = true
    App.Cache.aside(@aside).el.addClass("active")
    @el.on App.Constant.EVENT.transitionEnd, @constructor._onTransitionEnd
    if @el.attr("data-aside")
      @constructor._aside_open = false
      @el.addClass("aside").removeAttr("data-aside")
    else
      @constructor._aside_open = true
      @el.addClass("aside").attr("data-aside", "show")




class App.Atom.Label extends Atomic.Class

  @type: "Label"
  @template: """
    <label class="{{class}}">{{text}}</label>
  """

class App.Atom.Input extends Atomic.Class

  @type: "Input"
  @template: """
    <input type="{{type}}" placeholder="{{placeholder}}" class="{{class}}" name="{{name}}" />
  """

  value: -> @el.val()

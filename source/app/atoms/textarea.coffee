class App.Atom.Textarea extends Atomic.Class

  @type: "Textarea"
  @template: """
    <textarea class="{{class}}"></textarea>
  """

  value: -> @el.val()

class App.Atom.Li extends Atomic.Class

  @type: "Li"
  @template: """
    <li>
    	{{text}}
    	<small>{{description}}</small>
    </li>
  """

  constructor: ->
    super
    Commons.router.call(@)


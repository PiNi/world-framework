class App.Atom.P extends Atomic.Class

  @type: "Label"
  @template: """
    <p class="{{class}}">{{text}}</p>
  """

class App.Atom.Button extends Atomic.Class

  @type: "Button"
  @template: """
    <button class="{{class}}">
        <span class="icon {{icon}}"></span>
        {{text}}
    </button>
  """

  constructor: ->
    super
    Commons.router.call(@)
    Commons.data.call(@)

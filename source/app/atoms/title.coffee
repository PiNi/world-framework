class App.Atom.Title extends Atomic.Class

  @type: "Title"
  @template: """
    <h{{size}} class="{{class}}">{{text}}</h{{size}}>
  """

  constructor: ->
    super

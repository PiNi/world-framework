class App.Atom.Loading extends Atomic.Class

  @type: "Loading"
  @template: """
  	<div class="atom_loading">
  		<div class="">{{text}}</div>
  		<span class="icon looping"></span>
  	</div>
  """
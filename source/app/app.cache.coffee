App.Cache = do ->

  _cache = {}

  init = (app) ->
    _cacheCommons app

  setArticle = (instance) -> _cache.currentArticle = instance

  _cacheCommons = (app) ->
    for component in app when component.constructor.type in ["Article", "Aside"]
      node_name = component.constructor.type.toLowerCase()
      _cache[node_name] = _cache[node_name] or {}
      _cache[node_name][component.attributes.id] = component


  init        : init
  setArticle  : setArticle
  aside       : (id) -> _cache.aside[id]
  article     : (id) -> if id then _cache.article[id] else _cache.currentArticle

  register    : (namespace, item) ->
    _cache[namespace] = _cache[namespace] or {}
    _cache[namespace][item.attributes.id] = item

App.Router = do ->

  _routes = []

  _showArticle = (id, is_back, start_animation) -> ->
    instance = App.Cache.article(id)
    console.log instance
    instance.show(is_back, start_animation)
    if is_back then _routes.length--
    else _routes.push(id)

  init = (article_id, start_animation) ->
    article(article_id, false, start_animation)

  article = (id, is_back=false, start_animation) ->
    unless App.Template.Article._animations_in_progress > 0
      App.Modal.hide()
      funct = _showArticle(id, is_back, start_animation)
      if App.Template.Article._aside_open
        App.Cache.article().toggleAside(funct)
      else funct()

  section = (id) ->
    App.Modal.hide()
    App.Cache.article().section(id)

  modal = (id) ->
    if id is "hide" then App.Modal.hide()
    else App.Modal.show(id)

  aside = ->
    App.Cache.article().toggleAside()

  back = ->
    len = _routes.length
    if len <= 1 then return false
    article _routes[len - 2], true


  init        : init
  article     : article
  section     : section
  modal       : modal
  back        : back
  aside       : aside

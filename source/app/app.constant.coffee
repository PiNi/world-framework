App.Constant = do ->

  _width = window.innerWidth
  _height = window.innerHeight

  cssTransform = 
    'WebkitTransform'   : '-webkit-transform'
    'MozTransform'      : '-moz-transform'
    'OTransform'        : '-o-transform'
    'msTransform'       : '-ms-transform'
    'transform'         : 'transform'

  transitionFinishedEvents =
    'WebkitTransition'  : 'webkitTransitionEnd'
    'MozTransition'     : 'transitionend'
    'OTransition'       : 'oTransitionEnd'
    'msTransition'      : 'msTransitionEnd'
    'transition'        : 'transitionEnd'

  animationFinishedEvents =
    'WebkitAnimation'   : 'webkitAnimationEnd'
    'MozAnimation'      : 'animationend'
    'OAnimation'        : 'oAnimationEnd'
    'msAnimation'       : 'msAnimationEnd'
    'animation'         : 'animationEnd'

  isTouchScreen = Modernizr.touch


  TOUCH: isTouchScreen

  DEVICE:
    orientation : if _width > _height then 'portait' else 'landscape'
    width       : window.innerWidth
    height      : window.innerHeight

  EVENT:
    animationEnd  : animationFinishedEvents[Modernizr.prefixed("Animation")]
    transitionEnd : transitionFinishedEvents[Modernizr.prefixed("Transition")]
    touchStart    : if isTouchScreen then "touchstart" else "mousedown"
    touchMove     : if isTouchScreen then "touchmove" else "mousemove"
    touchEnd      : if isTouchScreen then "touchend" else "mouseup"
    
  CSS:
    transform     : cssTransform[Modernizr.prefixed("transform")]


App.Gesture.register ["swiping", "swipingHorizontal", "swipingVertical", "swipe"], do ->

    _first    = true
    _swiping  = false
    _swipingH = false
    _swipingV = false

    _log = (txt, clear) ->
      if clear then $("#page1 header h1").text(txt)
      else $("#page1 header h1").text($("#page1 header h1").text() + "; "+ txt)

    start: ->
      _first    = true
      _swiping  = false
      _swipingH = false
      _swipingV = false
      return true

    move: (event, gd) ->
      if gd.deltas.length is 1
        if not _swiping
          _swipingH = Math.abs(gd.deltas[0].x / gd.deltas[0].y) >= 2
          _swipingV = Math.abs(gd.deltas[0].y / gd.deltas[0].x) >= 2

        App.Gesture.trigger("swiping", first: _first)
        if _swipingH then App.Gesture.trigger("swipingHorizontal", first: _first)
        if _swipingV then App.Gesture.trigger("swipingVertical", first: _first)
        _swiping = true
        _first = false
      return true

    end: (event, gd) ->
      if _swiping then App.Gesture.trigger("swipe")
      return true

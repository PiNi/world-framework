class App.Molecule.List extends Atomic.Class

  @type: "List"
  @template: """
    <ul class="{{class}}"></ul>
  """
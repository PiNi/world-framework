class App.Molecule.Nav extends Atomic.Class

  @type: "Nav"
  @template: """
    <nav class="{{class}}"></nav>
  """
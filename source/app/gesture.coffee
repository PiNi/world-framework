App.Gesture = do ->

  STARTED = false

  _events = {}
  _callbacks = []
  _lastEvent = null
  _gesture = {}

  _touchstart = (event) ->
    _initialize(event)
    _handleEvent(event, "start")

  _touchmove = (event) ->
    return unless _gesture.startPositions
    _gesture.positions = _getFingersPositions(event)
    _gesture.deltaTime = new Date() - _gesture.startTime
    _setDeltaPositions()
    _handleEvent(event, "move")

  _touchend = (event) ->
    return unless _gesture.startPositions
    _gesture.deltaTime = new Date() - _gesture.startTime
    _handleEvent(event, "end")
    _initialize()

  _touchcancel = (event) ->
    _handleEvent(event, "cancel")
    _initialize()

  _initialize = (evt) ->
    _lastEvent = null
    _gesture = {}
    return unless evt?
    _gesture.startTime = new Date()
    _gesture.startPositions = _getFingersPositions(evt)

  _getFingersPositions = (evt) ->
    positions = []
    positions.push({x: touch.pageX, y: touch.pageY}) for touch in _getTouches(evt)
    positions

  _getTouches = (evt) ->
    if evt.touches? then evt.touches else [{pageX: evt.pageX, pageY: evt.pageY}]

  _setDeltaPositions = ->
    i = 0
    _gesture.deltas = []
    for position in _gesture.positions
      _gesture.deltas.push
        x: (position.x - _gesture.startPositions[i].x)
        y: (position.y - _gesture.startPositions[i++].y)
    _gesture.deltaTime = (new Date()) - _gesture.startTime

  _handleEvent = (event, moment) ->
    _lastEvent = event
    _gestureClone = JSON.parse(JSON.stringify(_gesture))
    for cb in _callbacks when !!cb[moment]
      cb[moment].call(null, event, _gestureClone)


  document.body.addEventListener(App.Constant.EVENT.touchStart, _touchstart)
  document.body.addEventListener(App.Constant.EVENT.touchMove, _touchmove)
  document.body.addEventListener(App.Constant.EVENT.touchEnd, _touchend)
  document.body.addEventListener("touchcancel", _touchcancel)


  register = (events, callbacks) ->
    _callbacks.push(callbacks)
    for evt in events
      _events[evt] = new CustomEvent(evt, {bubbles: true, cancelable: true})

  trigger = (event_name, data) ->
    evt = _events[event_name]
    evt.originalEvent = _lastEvent
    evt.gestureData = JSON.parse(JSON.stringify(_gesture))
    evt.gestureData[key] = data[key] for key of data when data
    _lastEvent.target.dispatchEvent(evt)

  asyncTrigger = (event_name, target, data) ->
    target.dispatchEvent(_events[event_name], data)


  register      : register
  trigger       : trigger
  asyncTrigger  : asyncTrigger



App = do ->

  init = (json_url, callback) ->
    Atomic.Loader.loadJson json_url, (app) ->
      App.Cache.init(app.structure)
      app_container = Atomic.DOM(document.body)
      component.render(app_container) for component in app.structure
      callback?.call(app)
      if app.startRoute
        App.Router.init(app.startRoute, app.startAnimation)


  init      : init
  Base      : Atomic.Class
  Atom      : {}
  Molecule  : {}
  Organism  : {}
  Template  : {}

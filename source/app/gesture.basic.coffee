App.Gesture.register ["tap", "hold", "doubleTap", "singleTap"], do ->

  HOLD_TIME   = 300
  TAP_TIME    = 300
  DOUBLE_TIME = 450
  GAP         = 3

  _possibleTap    = false
  _possibleDouble = false
  _holdTimeout    = null
  _simpleTimeout  = null

  start = (event, gd) ->
    _possibleTap = gd.startPositions.length is 1
    _holdTimeout = setTimeout(_lazyHold(event.target), HOLD_TIME)  if _possibleTap
    return

  move = (event, gd) ->
    delta = gd.deltas[0]
    _possibleTap = gd.deltas.length is 1 and Math.abs(delta.x) < GAP and Math.abs(delta.y) < GAP
    unless _possibleTap
      clearTimeout(_holdTimeout)
      clearTimeout(_simpleTimeout)
    return

  end = (event, gd) ->
    if _possibleTap and gd.deltaTime < TAP_TIME
      clearTimeout(_holdTimeout)
      App.Gesture.trigger("tap")
      if _possibleDouble
        clearTimeout(_simpleTimeout)
        App.Gesture.trigger("doubleTap")
        _possibleDouble = false
      else
        _simpleTimeout = setTimeout(_lazySimpleTap(event.target), DOUBLE_TIME)
        _possibleDouble = true
    return

  _lazyHold = (target) -> ->
    App.Gesture.asyncTrigger "hold", target

  _lazySimpleTap = (target) -> ->
    App.Gesture.asyncTrigger "singleTap", target
    _possibleDouble = false


  start: start
  move: move
  end: end
 

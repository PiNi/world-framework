Editor.Components = do ->

  _dragging_component = null

  _dragStart = (component, app_namespace) -> (evt) ->  
    _dragging_component = 
      component: component
      namespace: app_namespace

  _dragEnd = (evt) ->
    _dragging_component = null

  _createElement = (container, name) ->
    el = $("<li>#{name}</li>").attr("draggable", "true")
    container.append(el)
    return el

  _bindElementEvents = (el, component, app_namespace) ->
    el.on("dragstart", _dragStart(component, app_namespace))
    el.on("dragend", _dragEnd)

  load = (components_data) ->
    for type, components of components_data
      container = $("#NS_CONTAINER_#{type}")
      for name, component of components
        _bindElementEvents(_createElement(container, name), component, type)


  load        : load
  getCurrent  : -> _dragging_component

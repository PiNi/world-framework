Editor.Events = do ->

  listeners = {}

  bind = (ev, cb) ->
    listeners[ev] or= []
    listeners[ev].push(cb)

  trigger = (ev, data) ->
    if listeners[ev].length then for cb in listeners[ev]
      listeners[ev].call(listeners[ev], data)

  bind: bind
  trigger: trigger
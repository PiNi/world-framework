Editor.Loader = do ->

  _loaded = false
  _components = 
    "Template"  : {}
    "Organism"  : {}
    "Molecule"  : {}
    "Atom"      : {}

  getComponents = (force=false) ->
    if _loaded and !force then return _components
    for ns of _components
      _components[ns] = {}
      for component_name, compontent of App[ns]
        _components[ns][component_name] = compontent

    return _components


  getComponents: getComponents

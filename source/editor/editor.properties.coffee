Editor.Properties = do (C = Editor.Constants) ->

  RENDERER = templayed """
    <li>
      <div class="label">{{label}}</div>
      <div class="value">
        <input type="text" data-key="{{label}}" data-value-type="{{type}}" value="{{value}}" />
      </div>
    </li>
  """

  ELEMENTS = 
    header    : $("#aside_properties > header")
    section   : $("#aside_properties > section")
    footer    : $("#aside_properties > footer")
    attributes: $("#aside_properties > section ul#attributes")
    options   : $("#aside_properties > section ul#options")
    btn_delete: $("#aside_properties > footer button")

  _component = null

  # _mixObjectProperties = (type, namespace) ->
  #   obj = {}
  #   if C.COMPONENTS[namespace]
  #     for key in C.COMPONENTS[namespace][type]
  #       obj[key] = "" 
  #   obj

  _mixObjectProperties = (default_obj, obj={}) ->
    obj[key] or= "" for key in default_obj
    return obj

  _paintProperties = (type) ->
    container = ELEMENTS[type]
    container.find(":not(.title)").remove()
    if _component
      for key, value of _component[type]
        container.append $(RENDERER({
          type: type
          label: key
          value: value
        }))

  _onKeyUp = (evt) ->
    target = evt.currentTarget
    type = target.getAttribute("data-value-type")
    key = target.getAttribute("data-key")
    value = target.value.trim()
    _component[type][key] = value

  _onDelete = (evt) ->
    if _component
      Editor.Canvas.removeComponent(_component.uid)
      unselect()

  _setHeaderTitle = ->
    ELEMENTS.header.attr("data-component-type", _component.type)
    ELEMENTS.section.attr("data-component-type", _component.type)
    ELEMENTS.header.html(_component.type + " :: " + _component.name)

  unselect = ->
    _component = null
    ELEMENTS.btn_delete.attr("disabled", "disabled")
    _paintProperties("attributes")
    _paintProperties("options")

  select = (evt, component) ->
    if evt
      $(evt.currentTarget).addClass("selected")
      uid = evt.currentTarget.getAttribute(C.DATA_ATTRIBUTES.UID)
      component = Editor.Canvas.getComponent(uid)
      attributes = component.attributes
      options = component.options
      evt.stopPropagation()
      evt.preventDefault()

    if _component
      if _component.uid isnt component.uid
        _component.el.removeClass("selected")

    _component = component
    if !attributes? and !options?
      default_attributes = C.COMPONENTS[component.name].ATTRIBUTES
      default_options = C.COMPONENTS[component.name].OPTIONS
      component.attributes = _mixObjectProperties(default_attributes, component.attributes)
      component.options = _mixObjectProperties(default_options, component.options)
      console.log "Vaaa :: ", component.name, C.COMPONENTS

    console.log "Vaaa", component
    _setHeaderTitle()
    _paintProperties("attributes")
    _paintProperties("options")
    ELEMENTS.btn_delete.removeAttr("disabled")
    return evt?

  do ->
    Editor.Canvas.container.on("click", "[#{C.DATA_ATTRIBUTES.UID}]", select)
    ELEMENTS.section.on("keyup", "input[type=text]", _onKeyUp)
    ELEMENTS.btn_delete.on("click", _onDelete)


  select: select
  unselect: unselect


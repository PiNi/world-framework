Editor.Canvas = do (C = Editor.Constants) ->

  COMPONENT_RENDERER = templayed """
    <div 
      #{C.DATA_ATTRIBUTES.NAME}="{{name}}" 
      #{C.DATA_ATTRIBUTES.TYPE}="{{type}}"
      #{C.DATA_ATTRIBUTES.UID}="{{uid}}"
      >
      <span class="title">- {{name}}</span>
      <span class="id">{{attributes.id}}</span>
    </div>
  """

  _container      = $("#canvas")
  _components     = {}
  _componentsTree = []
  _lastDroppable  = null

  _closestParentSelector = (allowedParents) ->
    selector = []
    for component_name in allowedParents
      selector.push("[#{C.DATA_ATTRIBUTES.NAME}=\"#{component_name}\"]")
    return selector.join(",")

  _getClosestAllowedContainer = (el, name) ->
    ret = []
    if name
      allowedParents = C.COMPONENTS[name].CONTAINERS
      if allowedParents
        selector = _closestParentSelector(allowedParents)
        ret = el.closest(selector)
    return ret

  removeComponent = (uid) ->
    _components[uid].el.remove()
    delete _components[uid]
    regenerateTree()

  appendComponent = (component_data, container) ->
    uid = _guid()
    el = $(COMPONENT_RENDERER({
      name: component_data.component.name
      type: component_data.namespace
      uid: uid
      attributes: {}
      options: {}
    }))
    container.append(el)
    _components[uid] = {
      uid         : uid
      name        : component_data.component.name
      type        : component_data.namespace
      attributes  : component_data.component.attributes
      options     : component_data.component.options
      el          : el
    }
    return el

  _handleDrop = (ev) ->
    if _lastDroppable.length
      comp_data = Editor.Components.getCurrent()
      el = appendComponent(comp_data, _lastDroppable)
      el.trigger("click")
      regenerateTree()
    _onDraggingLeave(false)

  _onDraggingOver = (evt) ->
    component_data = Editor.Components.getCurrent()
    _lastDroppable = _getClosestAllowedContainer($(evt.target), component_data.component.name)
    _lastDroppable.addClass("droppable")
    evt.preventDefault()
    evt.stopPropagation()

  _onDraggingLeave = (evt) ->
    if _lastDroppable
      _lastDroppable.removeClass("droppable")
      _lastDroppable = null
      if evt
        evt.preventDefault()
        evt.stopPropagation()

  _guid = ->
    'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, (c) ->
      r = Math.random() * 16 | 0
      v = if c is 'x' then r else r & 3 | 8
      v.toString 16
    .toUpperCase()

  _getNodeTree = (parent_node) ->
    items = []
    for node in parent_node.childNodes when node.nodeName is "DIV"
      items.push({
        uid: node.getAttribute(C.DATA_ATTRIBUTES.UID)
        children: _getNodeTree(node)
      })
    return items

  regenerateTree = ->
    _componentsTree = _getNodeTree(_container[0])
    return true


  _container.attr(C.DATA_ATTRIBUTES.NAME, "__root__")
  _container.on("dragover", _onDraggingOver)
  _container.on("dragleave", _onDraggingLeave)
  _container.on("drop", _handleDrop)

  container       : _container
  appendComponent : appendComponent
  removeComponent : removeComponent
  regenerateTree  : regenerateTree
  getTree         : -> _componentsTree
  getComponents   : -> _components
  getComponent    : (uid) -> _components[uid]


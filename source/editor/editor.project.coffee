Editor.Project = do ->

  _components = {}
  _app_json = {}

  _getJsonTree = (tree_level) ->
    obj = {}
    component = _components[tree_level.uid]
    obj.name = component.name
    obj.type = component.type
    obj.attributes = component.attributes
    obj.options = component.options
    obj.children = []
    for child in tree_level.children
      obj.children.push(_getJsonTree(child))
    return obj

  _createTree = (component, container) ->
    el = Editor.Canvas.appendComponent({
      component: component
      namespace: component.type
    }, container)
    _createTree(child, el) for child in component.children

  _setBasics = ->
    for field in $("[data-modal=project_modal] [name]")
      _app_json[field.getAttribute("name")] = field.value

  edit = ->
    Editor.Modal.show("project_modal")

  save = ->
    _setBasics()
    _components = Editor.Canvas.getComponents()
    _app_json.structure = []
    for comp in Editor.Canvas.getTree()
      _app_json.structure.push(_getJsonTree(comp))

    localStorage.setItem(_app_json.title, JSON.stringify(_app_json))

    console.log _app_json
    
    textarea = $("[data-modal=json_modal] > textarea")
    json_str = JSON.stringify(_app_json)
    textarea.val(json_str)
    Editor.Modal.show("json_modal")

  load = (app_json) ->
    _app_json = app_json
    for component in app_json.structure
      _createTree(component, Editor.Canvas.container)
    Editor.Canvas.regenerateTree()
    for field in $("[data-modal=project_modal] [name]")
      field.value = _app_json[field.getAttribute("name")]


  $ -> 
    $("[data-action=save]").on("click", save)
    $("[data-action=edit]").on("click", edit)


  save: save
  load: load


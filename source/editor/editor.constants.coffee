Editor.Constants =

  DATA_ATTRIBUTES: 
    NAME: "data-component-name"
    TYPE: "data-component-type"
    UID:  "data-component-uid"

  COMPONENTS:

    # Templates
    "Article":
      CONTAINERS  : ["__root__"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : ["animation", "aside"]

    # Organisms
    "Aside": 
      CONTAINERS  : ["__root__"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "Modal": 
      CONTAINERS  : ["__root__"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "Section": 
      CONTAINERS  : ["Article", "Aside"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : ["pullable"]

    # Molecules
    "Header": 
      CONTAINERS  : ["Article", "Aside"]
      ATTRIBUTES  : ["id", "class", "title"]
      OPTIONS     : []
    "Form":
      CONTAINERS  : ["Section"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "List":
      CONTAINERS  : ["Section"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "Nav":
      CONTAINERS  : ["Section", "Header", "Footer"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "Search":
      CONTAINERS  : ["Section", "Header", "Footer"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "Footer":
      CONTAINERS  : ["Article", "Aside"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []

    # Atoms
    "Button":
      CONTAINERS  : ["Section", "Form", "Nav", "Header", "Footer"]
      ATTRIBUTES  : ["id", "class", "text", "icon", "container"]
      OPTIONS     : ["router"]
    "Input":
      CONTAINERS  : ["Section", "Form"]
      ATTRIBUTES  : ["id", "class", "placeholder", "value"]
      OPTIONS     : []
    "Label":
      CONTAINERS  : ["Section", "Form"]
      ATTRIBUTES  : ["id", "class", "text"]
      OPTIONS     : []
    "Li":
      CONTAINERS  : ["List"]
      ATTRIBUTES  : ["id", "class", "text", "description"]
      OPTIONS     : ["router"]
    "Loading":
      CONTAINERS  : ["Section"]
      ATTRIBUTES  : ["id", "class", "text"]
      OPTIONS     : []
    "P":
      CONTAINERS  : ["Section", "Form"]
      ATTRIBUTES  : ["id", "class", "text"]
      OPTIONS     : []
    "Select":
      CONTAINERS  : ["Section", "Form"]
      ATTRIBUTES  : ["id", "class"]
      OPTIONS     : []
    "Textarea":
      CONTAINERS  : ["Section", "Form"]
      ATTRIBUTES  : ["id", "class", "placeholder", "value"]
      OPTIONS     : []
    "Title":
      CONTAINERS  : ["Section", "Form"]
      ATTRIBUTES  : ["id", "class", "text"]
      OPTIONS     : []


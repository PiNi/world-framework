Editor.Modal = do ->

  ANIMATION_END = App.Constant.EVENT.animationEnd

  _current = null
  _shadow = $("[data-modal-shadow]")

  _onShowEnd = (ev) ->
    _current.removeClass("showing").addClass("show")
    _current.off(ANIMATION_END, _onShowEnd)

  _onHideEnd = (ev) ->
    _current.removeClass("hidding")
    _current.off(ANIMATION_END, _onHideEnd)
    _current = null

  show = (id) ->
    _current = $("[data-modal='#{id}']")
    _current.on(ANIMATION_END, _onShowEnd)
    _current.addClass("showing")
    _shadow.addClass("show")
    true


  hide = () ->
    _current.on(ANIMATION_END, _onHideEnd)
    _current.removeClass("show").addClass("hidding")
    _shadow.removeClass("show")
    true

  $ -> $("button[data-action=close_modal]").on("click", hide)

  show: show
  hide: hide